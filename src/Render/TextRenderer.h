#pragma once

#include <common.h>
#include <Render\Resource\Font.h>
#include <glm.h>

class ShaderProgram;

class TextRenderer
{
	struct Glyph
	{
		float pos, width;
		int color;
		char16_t c;
	};

	unsigned int vao, vbo, uniformMVP, unifromMinAlpha, uniformMaxAlpha;
	unsigned int uniformOffset, uniformSize, uniformMinCorner, uniformMaxCorner;
	unsigned int uniformUseBounds;
	Font font;
	shared_ptr<ShaderProgram> fontProgram;
	mat4 model;

	TextRenderer();
	TextRenderer(const TextRenderer &);
	TextRenderer & operator=(TextRenderer &);

	int color = -1;
	unsigned char glyphWidth[65536];

	void RenderString(u16string str);
	u16string FormatString(string format, va_list list);
	float size = 12;
	float baselinePos = 190.0f / 255.0f;
	int minx = -1, maxx = -1;
	int miny = -1, maxy = -1;
	bool useBounds = false;
public:

	static TextRenderer & GetInstance();

	void RenderStringFormat(float x, float y, string format, ...);
	void RenderStringFormatBounded(float x, float y, string format, ...);
	u16string FormatString(string format, ...);
	void SetColor(float r, float g, float b, float a = 1);
	void SetSize(float size);
	float GetBaseLinePosition(float size = 0);
	void ReloadFont();
	float GetStringWidth(u16string str);
};