#include "ShaderProgram.h"

ShaderProgram::ShaderProgram()
{
	id = -1;
}

ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(id);
}

shared_ptr<ShaderProgram> ShaderProgram::CreateFromFile(string fileName)
{
	ShaderProgram * program = new ShaderProgram();

	program->file = fileName;
	program->Reload();

	return shared_ptr<ShaderProgram>(program);
}

bool ShaderProgram::Reload()
{
	ifstream f;
	f.open(file, ios::in);
	string source;
	source.assign(istreambuf_iterator<char>(f), istreambuf_iterator<char>());
	f.close();

	unsigned int id = glCreateProgram();

	vector<int> shaderMarkers;
	vector<int> shaders;

	int successful = true;

	for (unsigned int i = 0; i < shaderTags.size(); i++)
	{
		string shaderTag = get<0>(shaderTags[i]);

		shaderMarkers.push_back(source.find(shaderTag));

		if (shaderMarkers[i] > 2)
		{
			string tmp = source.substr(shaderMarkers[i] - 2, 2);
			if (tmp == "//")
			{
				source.replace(shaderMarkers[i] - 2, 1, 1, '\0');
				shaderMarkers[i] = -1;
			}
		}

		if (shaderMarkers[i] != -1)
			source.replace(shaderMarkers[i], shaderTag.length(), shaderTag.length(), '\0');
	}

	for (unsigned int i = 0; i < shaderTags.size(); i++)
	{
		if (shaderMarkers[i] != -1)
		{
			shaders.push_back(glCreateShader(get<1>(shaderTags[i])));

			unsigned int shader = shaders.back();

			string src = source;
			auto srcStart = src.begin() + shaderMarkers[i] + get<0>(shaderTags[i]).length();

			src.erase(remove_if(src.begin(), srcStart, [] (char c)
			{
				return c != '\n';
			}), srcStart);

			const char * str = src.c_str();

			glShaderSource(shader, 1, &str, nullptr);

			glCompileShader(shader);

			int status = GL_FALSE;
			glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

			if (status != GL_TRUE)
			{
				int len = 0;
				glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);

				char * log = new char[len];
				glGetShaderInfoLog(shader, len, nullptr, log);

				cout << "Shader " << get<0>(shaderTags[i]) << id << " compilation errors : " << endl;

				cout << log << endl << endl;
				delete log;

				successful = false;
			}
		}
	}

	if (!successful)
	{
		cout << "Some of the shaders in program " << id << " from " << file << " has compilation errors. Program isn't loaded." << endl << endl << endl;
		glDeleteProgram(id);
		return false;
	}

	for (int shader : shaders)
		glAttachShader(id, shader);

	glLinkProgram(id);

	for (int shader : shaders)
	{
		glDetachShader(id, shader);
		glDeleteShader(shader);
	}

	shaders.clear();

	int status = GL_FALSE;
	glGetProgramiv(id, GL_LINK_STATUS, &status);

	if (status != GL_TRUE)
	{
		int len = 0;
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &len);

		char * log = new char[len];
		glGetProgramInfoLog(id, len, nullptr, log);

		cout << "Program " << id << " linking errors : " << endl;
		cout << log << endl << endl;

		delete log;

		cout << "Program " << id << " from " << file << "has link errors. Program isn't loaded." << endl << endl << endl;
		glDeleteProgram(id);

		return false;
	}

	if (this->id != -1)
		glDeleteProgram(this->id);
	this->id = id;

	return true;
}

void ShaderProgram::Bind()
{
	glUseProgram(id);
}

unsigned int ShaderProgram::GetUniformLocation(string uniformname)
{
	return glGetUniformLocation(id, uniformname.c_str());
}