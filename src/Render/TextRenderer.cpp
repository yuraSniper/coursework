#include "TextRenderer.h"

#include <Render\Resource\ResourceManager.h>
#include <Render\Shader\ShaderProgram.h>
#include <Gui\GuiArea.h>
#include <Main\Main.h>

TextRenderer & TextRenderer::GetInstance()
{
	static TextRenderer instance;
	return instance;
}

TextRenderer::TextRenderer()
{
	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &vao);

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(0, 2, GL_FLOAT, false, sizeof(Glyph), (void *) offsetof(Glyph, pos));
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, true, sizeof(Glyph), (void *) offsetof(Glyph, color));
	glVertexAttribIPointer(2, 1, GL_UNSIGNED_SHORT, sizeof(Glyph), (void *) offsetof(Glyph, c));
	
	glBindVertexArray(0);
	font = ResourceManager::GetInstance().LoadFont("data\\Font");
	fontProgram = ShaderProgram::CreateFromFile("data\\Shaders\\Font.glsl");

	ReloadFont();
}

void TextRenderer::RenderString(u16string str)
{
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	vector<Glyph> data;

	float pos = 0;
	for (int i = 0; i < str.length(); i++)
	{
		if (str[i] != 32)
		{
			float charWidth = glyphWidth[str[i]] / 32.0f;
			float position = pos;
			if (!font.pages[str[i] >> 8])
				ResourceManager::GetInstance().LoadFontPage(font, str[i] >> 8);
			float tmp = 18.0f / 32.0f - charWidth;
			if (tmp > 0)
				position += tmp * size / 2.0f;
			
			data.push_back({position, charWidth, color, str[i]});
		}
		pos += 18.0f * size / 32.0f;
	}

	glBufferData(GL_ARRAY_BUFFER, sizeof(Glyph) * data.size(), data.data(), GL_STREAM_DRAW);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0);
	
	glDrawArrays(GL_POINTS, 0, data.size());

	glDisable(GL_ALPHA_TEST);
	glDisable(GL_BLEND);
}

u16string TextRenderer::FormatString(string format, va_list list)
{
	wchar_t str[512];

	wchar_t tmp[512];

	for (int i = 0; i < format.length(); i++)
		tmp[i] = format[i];

	tmp[format.length()] = 0;

	vswprintf(str, tmp, list);

	u16string result;
	for (int i = 0; i < wcslen(str); i++)
		result.push_back(str[i]);

	return result;
}

u16string TextRenderer::FormatString(string format, ...)
{
	va_list list;

	va_start(list, format);
	u16string result = FormatString(format, list);
	va_end(list);

	return result;
}

void TextRenderer::RenderStringFormat(float x, float y, string format, ...)
{
	va_list list;

	va_start(list, format);
	u16string result = FormatString(format, list);
	va_end(list);

	y -= baselinePos * size;

	int prevProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prevProgram);

	fontProgram->Bind();
	Main & renderer = Main::GetInstance();
	glUniformMatrix4fv(uniformMVP, 1, false, value_ptr(renderer.GetMVP()));
	glUniform2f(uniformOffset, x, y);
	glUniform1f(uniformSize, size);
	glUniform1i(uniformUseBounds, !useBounds);

	if (useBounds)
	{
		glUniform2f(uniformMinCorner, minx, miny);
		glUniform2f(uniformMaxCorner, maxx, maxy);
	}

	float alphaThreshold;
	if (size < 32)
		alphaThreshold = mix(0.8f, 0.4f, (size - 12.0f) / 20.0f);
	else if (size < 64)
		alphaThreshold = mix(0.4f, 0.2f, (size - 32.0f) / 32.0f);
	else if (size < 128)
		alphaThreshold = mix(0.2f, 0.1f, (size - 64.0f) / 64.0f);
	else if (size < 256)
		alphaThreshold = mix(0.1f, 1 / 16.0f, (size - 128.0f) / 128.0f);
	else
		alphaThreshold = glm::fastInverseSqrt(size);

	alphaThreshold /= 2;
	glUniform1f(unifromMinAlpha, 0.5 - alphaThreshold);
	glUniform1f(uniformMaxAlpha, 0.5 + alphaThreshold);

	RenderString(result);

	glBindVertexArray(0);
	glUseProgram(prevProgram);
}

void TextRenderer::RenderStringFormatBounded(float x, float y, string format, ...)
{
	va_list list;

	va_start(list, format);
	u16string result = FormatString(format, list);
	va_end(list);

	GuiArea bb = GuiArea::GetClampedParent();
	ivec2 absOffset = GuiArea::GetAbsOffset();
	float width = GetStringWidth(result);
	float remainderWidth = x + width - bb.width;
	if (remainderWidth > 0)
	{
		int pos = floor((width - remainderWidth) * 32.0f / (18.0f * size));
		if (pos > 0)
		{
			result = result.substr(0, pos - 1);
			result.append(u"�");
		}
		else
			return;
	}
	
	bb.x = bb.y = 0;
	bb = bb.MakeAbsolute();

	minx = bb.x;
	maxx = bb.x + bb.width - 1;
	miny = bb.y;
	maxy = bb.y + bb.height - 1;

	useBounds = true;

	RenderStringFormat(GuiArea::GetAbsOffset().x + x, GuiArea::GetAbsOffset().y + y, "%s", result.c_str());

	useBounds = false;
}

void TextRenderer::SetColor(float r, float g, float b, float a)
{
	r *= 255.0f;
	g *= 255.0f;
	b *= 255.0f;
	a *= 255.0f;

	color = ((int) r << 24) | ((int) g << 16) | ((int) b << 8) | (int) a;
}

void TextRenderer::SetSize(float size)
{
	this->size = glm::max(12.0f, size);
}

float TextRenderer::GetBaseLinePosition(float size)
{
	if (size == 0)
		size = this->size;

	return baselinePos * size;
}

void TextRenderer::ReloadFont()
{
	int prevProgram;
	glGetIntegerv(GL_CURRENT_PROGRAM, &prevProgram);
	fontProgram->Reload();
	for (int i = 0; i < 255; i++)
		font.pages[i] = false;

	FILE * file = fopen("data\\Font\\font.fnt", "rb");
	fread(glyphWidth, sizeof(unsigned char), 65536, file);
	fclose(file);

	fontProgram->Bind();
	glUniform1i(fontProgram->GetUniformLocation("tex"), 1);
	uniformMVP = fontProgram->GetUniformLocation("mvp");
	unifromMinAlpha = fontProgram->GetUniformLocation("minAlpha");
	uniformMaxAlpha = fontProgram->GetUniformLocation("maxAlpha");
	uniformOffset = fontProgram->GetUniformLocation("offset");
	uniformSize = fontProgram->GetUniformLocation("size");
	uniformMinCorner = fontProgram->GetUniformLocation("minCorner");
	uniformMaxCorner = fontProgram->GetUniformLocation("maxCorner");
	uniformUseBounds = fontProgram->GetUniformLocation("useBounds");

	glUseProgram(prevProgram);

	font.Bind(1);
}

float TextRenderer::GetStringWidth(u16string str)
{
	return 18.0f * size * str.length() / 32.0f;
}