#pragma once

#include <common.h>

struct Vertex
{
	float x, y, z;
	float s, t;
	int color;
	short atlas, texture;
	Vertex(float x = 0, float y = 0, float z = 0, float u = 0, float v = 0, short atlas = 0, short texture = 0, int color = 0xFFFFFFFF)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->s = u;
		this->t = v;
		this->atlas = atlas;
		this->texture = texture;
		this->color = _byteswap_ulong(color);
	}
};

class RenderBuffer
{
	unsigned int vbo = -1;
	unsigned int vao = -1;
	bool updated = true;
	int size = 0;

	vector<Vertex> buffer;
public:
	~RenderBuffer();

	void AddVertex(Vertex & vert);
	void AddVertices(Vertex * verts, int count);

	void Draw(unsigned int mode);
	void Clear();
};