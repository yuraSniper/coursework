#include "RenderBuffer.h"

RenderBuffer::~RenderBuffer()
{
	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);
}

void RenderBuffer::AddVertex(Vertex & vert)
{
	buffer.push_back(vert);
	size++;
	updated = true;
}

void RenderBuffer::AddVertices(Vertex * verts, int count)
{
	for (int i = 0; i < count; i++)
		buffer.push_back(verts[i]);
	size += count;
	updated = true;
}

void RenderBuffer::Draw(unsigned int mode)
{
	if (size == 0)
		return;

	if (vao == -1)
	{
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex), (void *) offsetof(Vertex, x));
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, true, sizeof(Vertex), (void *) offsetof(Vertex, color));
		glVertexAttribPointer(2, 2, GL_FLOAT, false, sizeof(Vertex), (void *) offsetof(Vertex, s));
		glVertexAttribIPointer(3, 2, GL_UNSIGNED_SHORT, sizeof(Vertex), (void *) offsetof(Vertex, atlas));

		glBindVertexArray(0);
	}

	if (updated)
	{
		size = buffer.size();
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, size * sizeof(Vertex), buffer.data(), GL_STREAM_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		buffer.clear();
		updated = false;
	}

	glBindVertexArray(vao);
	glDrawArrays(mode, 0, size);
	glBindVertexArray(0);
}

void RenderBuffer::Clear()
{
	size = 0;
	updated = true;
	buffer.clear();
}