#include "ResourceManager.h"

#include <Render\Resource\TextureAtlas.h>
#include <Render\Resource\Texture.h>

ResourceManager & ResourceManager::GetInstance()
{
	static ResourceManager instance;
	return instance;
}

Texture ResourceManager::LoadTextureNotFound(int size)
{
	PNGTex blank;
	blank.size = size;
	blank.data = new vector<unsigned char>(size * size * 4);

	for (int i = 0; i < blank.data->capacity(); i++)
		(*blank.data)[i] = 0xFF;

	return StoreTexture(blank);
}

ResourceManager::ResourceManager()
{
	int maxTexSize;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTexSize);
	atlasSize = min(1024, maxTexSize);

	glGenTextures(1, &atlasArrayId);
	glActiveTexture(GL_TEXTURE2);

	glBindTexture(GL_TEXTURE_2D_ARRAY, atlasArrayId);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA8, atlasSize, atlasSize, 0, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glActiveTexture(GL_TEXTURE0);

	glGenFramebuffers(1, &tmpFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, tmpFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

ResourceManager::PNGTex ResourceManager::LoadTextureFromFile(string path, int type)
{
	PNGTex tex;
	tex.data = new vector<unsigned char>;

	vector<unsigned char> buffer;

	ifstream file;
	file.open(path, ios::in | ios::binary);
	buffer.assign(istreambuf_iterator<char>(file), istreambuf_iterator<char>());
	file.close();

	lodepng::State state;
	state.info_raw.colortype = (LodePNGColorType) type;

	lodepng::decode(*tex.data, tex.size, tex.size, state, buffer);

	return tex;
}

Texture ResourceManager::StoreTexture(PNGTex tex)
{
	Texture texture = StoreTextureInFreeAtlas(tex);

	if (texture.GetAtlasId() != -1)
		return texture;

	if (atlasSize < tex.size)
		return texture;

	TextureAtlas atlas = AllocateAtlasInArray(TextureAtlas(-1, atlasSize, tex.size));
	textureAtlases.push_back(atlas);

	return StoreTextureInFreeAtlas(tex);
}

void ResourceManager::StoreTextureInAtlas(Texture texture, PNGTex tex)
{
	TextureAtlas atlas = textureAtlases[texture.GetAtlasId()];
	int atlasTexSize = atlas.GetTexureSize();

	int row = (texture.GetAtlasSlot() / atlas.GetTexturesInRow());
	int column = texture.GetAtlasSlot() % atlas.GetTexturesInRow();

	glActiveTexture(GL_TEXTURE2);
	glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, column * atlasTexSize,
		row * atlasTexSize, atlas.GetId(), atlasTexSize, atlasTexSize,
		1, GL_RGBA, GL_UNSIGNED_BYTE, tex.data->data());
	glActiveTexture(GL_TEXTURE0);

	textureAtlases[texture.GetAtlasId()].DecrementFreeSlots();
}

Texture ResourceManager::StoreTextureInFreeAtlas(PNGTex texture)
{
	for (int i = 0; i < textureAtlases.size(); i++)
	{
		TextureAtlas atlas = textureAtlases[i];
		int atlasTexSize = atlas.GetTexureSize();
		if (atlas.GetFreeSlots() > 0 && atlasTexSize == texture.size)
		{
			int slot = atlas.GetMaxSlots() - atlas.GetFreeSlots();

			Texture tex(i, textures.size(), slot, atlas.GetId());
			textures.push_back(tex);

			StoreTextureInAtlas(tex, texture);

			delete texture.data;

			return tex;
		}
	}
	return Texture(-1, 0);
}

TextureAtlas ResourceManager::AllocateAtlasInArray(TextureAtlas atlas)
{
	unsigned int tmpArray;
	glGenTextures(1, &tmpArray);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D_ARRAY, tmpArray);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA8, atlasSize, atlasSize, atlasCount + 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

	glBindFramebuffer(GL_FRAMEBUFFER, tmpFBO);

	for (int i = 0; i < atlasCount; i++)
	{
		glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, atlasArrayId, 0, i);
		glReadBuffer(GL_COLOR_ATTACHMENT0);
		glCopyTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, 0, 0, atlasSize, atlasSize);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDeleteTextures(1, &atlasArrayId);
	atlasArrayId = tmpArray;
	
	glActiveTexture(GL_TEXTURE0);
	atlasCount++;

	return TextureAtlas(atlasCount - 1, atlas.GetSize(), atlas.GetTexureSize());
}

Texture ResourceManager::LoadTexture(string path)
{
	PNGTex tex = LoadTextureFromFile(path);

	if (atlasCount == 0)
		LoadTextureNotFound(tex.size);

	return StoreTexture(tex);
}

TextureAtlas ResourceManager::GetAtlasInfo(int atlas)
{
	return textureAtlases[atlas];
}

Font ResourceManager::LoadFont(string path)
{
	unsigned int id;
	glGenTextures(1, &id);

	glBindTexture(GL_TEXTURE_2D_ARRAY, id);

	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_LUMINANCE8, 512, 512, 256, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, nullptr);

	Font font = Font(id, 512, path);
	for (int i = 0; i < 256; i++)
		font.pages[i] = false;

	FILE * file = fopen((path + "\\font.fnt").c_str(), "rb");
	fread(font.glyphWidth, sizeof(unsigned char), 65536, file);
	fclose(file);

	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

	return font;
}

void ResourceManager::LoadFontPage(Font & font, unsigned char page)
{
	char str[3];
	itoa(page, str, 16);
	PNGTex texture = LoadTextureFromFile(font.path + "\\page_" + str + ".png", LCT_GREY);

	if (!texture.data->empty())
	{
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, page, 512, 512, 1, GL_LUMINANCE, GL_UNSIGNED_BYTE, &((*texture.data)[0]));
		delete texture.data;
		font.pages[page] = true;
	}
}