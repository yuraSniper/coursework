#include "TextureAtlas.h"

TextureAtlas::TextureAtlas(unsigned int id, int size, int textureSize)
{
	this->id = id;
	this->size = size;
	this->textureSize = textureSize;
	this->freeSlots = GetMaxSlots();
}

unsigned int TextureAtlas::GetId()
{
	return id;
}

int TextureAtlas::GetSize()
{
	return size;
}

int TextureAtlas::GetTexureSize()
{
	return textureSize;
}

int TextureAtlas::GetTexturesInRow()
{
	return size / textureSize;
}

int TextureAtlas::GetMaxSlots()
{
	return GetTexturesInRow() * GetTexturesInRow();
}

int TextureAtlas::GetFreeSlots()
{
	return freeSlots;
}

bool TextureAtlas::DecrementFreeSlots()
{
	if (freeSlots == 0)
		return false;
	freeSlots--;
	return true;
}