#include "Texture.h"

Texture::Texture(int textureAtlasRegistryId, int registryId, int slot, int layer)
{
	this->textureAtlasRegistryId = textureAtlasRegistryId;
	this->registryId = registryId;
	this->slot = slot;
	this->layer = layer;
}

int Texture::GetId()
{
	return registryId;
}

int Texture::GetAtlasId()
{
	return textureAtlasRegistryId;
}

int Texture::GetAtlasSlot()
{
	return slot;
}

int Texture::GetAtlasLayer()
{
	return layer;
}