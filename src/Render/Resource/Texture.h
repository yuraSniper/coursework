#pragma once

#include <common.h>

class Texture
{
	int textureAtlasRegistryId, slot, layer;
	int registryId;

public:
	Texture(int textureAtlasRegistryId = -1, int registryId = -1, int slot = 0, int layer = 0);

	int GetId();
	int GetAtlasId();
	int GetAtlasSlot();
	int GetAtlasLayer();
};