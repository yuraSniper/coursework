#include "Font.h"

Font::Font(unsigned int id, int size, string path) : TextureAtlas(id, size, size / 16)
{
	freeSlots = 0;
	this->path = path;
}

void Font::Bind(int location)
{
	glActiveTexture(GL_TEXTURE0 + location);
	glBindTexture(GL_TEXTURE_2D_ARRAY, GetId());
}