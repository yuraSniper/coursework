#pragma once

#include <common.h>

#include <Render\Resource\TextureAtlas.h>
#include <Render\Resource\Texture.h>
#include <Render\Resource\Font.h>

class ResourceManager
{
	struct PNGTex
	{
		unsigned int size;
		vector<unsigned char> * data;
	};

	int atlasSize;

	ResourceManager();
	ResourceManager(const ResourceManager &);
	ResourceManager & operator=(ResourceManager &);

	int atlasCount = 0;
	unsigned int atlasArrayId, tmpFBO;
	vector<TextureAtlas> textureAtlases;
	vector<Texture> textures;

	PNGTex LoadTextureFromFile(string path, int type = LCT_RGBA);
	Texture StoreTexture(PNGTex texture);
	void StoreTextureInAtlas(Texture texture, PNGTex tex);
	Texture StoreTextureInFreeAtlas(PNGTex tex);
	TextureAtlas AllocateAtlasInArray(TextureAtlas atlas);

public:
	static ResourceManager & GetInstance();

	Texture LoadTextureNotFound(int size);
	Texture LoadTexture(string path);
	TextureAtlas GetAtlasInfo(int atlas);

	Font LoadFont(string path);
	void LoadFontPage(Font & font, unsigned char page);
};