#pragma once

#include <common.h>

class TextureAtlas
{
	unsigned int id;
	int size, textureSize;
protected:
	int freeSlots;

public:
	TextureAtlas(unsigned int id, int size, int textureSize);

	unsigned int GetId();
	int GetSize();
	int GetTexureSize();
	int GetMaxSlots();
	int GetTexturesInRow();
	int GetFreeSlots();
	bool DecrementFreeSlots();
};