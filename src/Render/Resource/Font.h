#pragma once

#include <common.h>
#include <Render\Resource\TextureAtlas.h>

class Font : public TextureAtlas
{
public:
	bool pages[256];
	unsigned char glyphWidth[65536];
	string path;

	Font(unsigned int id = -1, int size = 512, string path = "");

	void Bind(int location = 1);
};