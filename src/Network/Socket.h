#pragma once

#include <common.h>

class NetworkHelper;

class Socket
{
public:
	Socket();
	SOCKET s;
	bool Connect(string ip, unsigned short port);
	void Disconnect();
	bool IsConnected();
	bool IsValid();

	string GetIpAndPort();
	
	void Send(void * buffer, int size);
	void Send(vector<unsigned char> & buffer);
	void Send(u16string str);
	void Send(string str);
	int Receive(void * buffer, int bufferSize);
	vector<unsigned char> Receive();
	void Receive(string & str);
	void Receive(u16string & str);
	bool CanReceive();
};