#pragma once

#include <common.h>
#include <Network\Socket.h>

class NetworkHelper
{
	static SOCKET listener;
	static SOCKET udpSocket;
public:
	static void Startup(unsigned short listenerPort = 0);
	static void Shutdown();
	
	static Socket AcceptConnection();

	static void BindUDPSocketToPort(unsigned short port);
	static void SendUDPPacket(string to, unsigned short port, void * buffer, int size);
	static int ReceiveUDPPacket(void * buffer, int size, string & senderIp, unsigned short & senderPort);
	static bool CanReceiveUDPPacket();
};