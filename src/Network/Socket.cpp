#include "Socket.h"

Socket::Socket()
{
	s = -1;
}

bool Socket::Connect(string ip, unsigned short port)
{
	if (!IsConnected())
	{
		HOSTENT *host;
		sockaddr_in addr;

		host = gethostbyname(ip.c_str());
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr.S_un.S_addr = *((unsigned long*) host->h_addr_list[0]);

		if (s != -1)
			closesocket(s);
		s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		return !connect(s, (sockaddr *) &addr, sizeof(sockaddr_in));
	}

	return true;
}

void Socket::Disconnect()
{
	shutdown(s, SD_BOTH);
	closesocket(s);
	s = -1;
}

bool Socket::IsConnected()
{
	if (CanReceive())
	{
		char b;
		int result = recv(s, &b, 1, MSG_PEEK);
		int err = WSAGetLastError();
		if (result == 0 || err == WSAECONNRESET || err == WSAENOTCONN || err == WSAENOTSOCK)
			return false;
	}

	return true;
}

bool Socket::IsValid()
{
	return s != -1;
}

string Socket::GetIpAndPort()
{
	char str[32];
	unsigned long length = 32;
	sockaddr addr;
	int size = sizeof(sockaddr);
	getpeername(s, &addr, &size);
	WSAAddressToStringA(&addr, size, nullptr, str, &length);

	return string(str);
}

void Socket::Send(void * buffer, int size)
{
	if (size > 0)
		send(s, (char *) buffer, size, 0);
}

void Socket::Send(vector<unsigned char> & buffer)
{
	int size = buffer.size();
	Send(&size, sizeof(int));
	Send(buffer.data(), size);
}

void Socket::Send(string str)
{
	vector<unsigned char> buffer;
	buffer.assign(str.c_str(), str.c_str() + str.length());
	Send(buffer);
}

void Socket::Send(u16string str)
{
	vector<unsigned char> buffer;
	buffer.assign((char *)str.c_str(), (char *)str.c_str() + str.length() * 2);
	Send(buffer);
}

int Socket::Receive(void * buffer, int bufferSize)
{
	if (bufferSize == 0)
		return 0;
	
	int size = recv(s, (char *)buffer, bufferSize, 0);
	return size;
}

vector<unsigned char> Socket::Receive()
{
	vector<unsigned char> buffer;
	int size = 0;
	char *buf;
	
	if (Receive(&size, sizeof(int)) <= 0)
		return buffer;

	buf = new char[size];
	Receive(buf, size);

	buffer.assign(buf, buf + size);
	delete buf;

	return buffer;
}

void Socket::Receive(string & str)
{
	vector<unsigned char> buf = Receive();
	str.assign((char *) buf.data(), buf.size());
}

void Socket::Receive(u16string & str)
{
	vector<unsigned char> buf = Receive();
	str.assign((char16_t *)buf.data(), buf.size() / 2);
}

bool Socket::CanReceive()
{
	fd_set fdSet;
	FD_ZERO(&fdSet);
	FD_SET(s, &fdSet);
	TIMEVAL t = {0, 0};
	select(0, &fdSet, nullptr, nullptr, &t);

	return FD_ISSET(s, &fdSet);
}