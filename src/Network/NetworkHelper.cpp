#include "NetworkHelper.h"

SOCKET NetworkHelper::listener;
SOCKET NetworkHelper::udpSocket;

void NetworkHelper::Startup(unsigned short listenerPort)
{
	WSAData data;
	WSAStartup(WINSOCK_VERSION, &data);
	listener = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	udpSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	char a = 1;
	setsockopt(udpSocket, SOL_SOCKET, SO_BROADCAST, &a, sizeof(char));

	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(listenerPort);
	addr.sin_addr.S_un.S_addr = INADDR_ANY;
	bind(listener, (sockaddr *) &addr, sizeof(sockaddr_in));
}

void NetworkHelper::Shutdown()
{
	WSACleanup();
}

Socket NetworkHelper::AcceptConnection()
{
	Socket s;
	if (listen(listener, 10) == -1)
		cout << "Listen error : " << WSAGetLastError() << endl;
	if ((s.s = accept(listener, nullptr, nullptr)) == -1)
		cout << "Accept error : " << WSAGetLastError() << endl;

	return s;
}

void NetworkHelper::BindUDPSocketToPort(unsigned short port)
{
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;
	bind(udpSocket, (sockaddr *) &addr, sizeof(sockaddr_in));
}

void NetworkHelper::SendUDPPacket(string to, unsigned short port, void * buffer, int size)
{
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(to.c_str());
	addr.sin_port = htons(port);

	sendto(udpSocket, (char *)buffer, size, 0, (sockaddr *)&addr, sizeof(addr));
}

int NetworkHelper::ReceiveUDPPacket(void * buffer, int size, string & senderIp, unsigned short & senderPort)
{
	if (!CanReceiveUDPPacket())
		return 0;

	int addrSize = sizeof(sockaddr_in);
	sockaddr_in sender;
	int msgSize = recvfrom(udpSocket, (char *)buffer, size, 0, (sockaddr *)&sender, &addrSize);

	char ip[32];
	unsigned long ipSize = 32;
	WSAAddressToStringA((sockaddr *)&sender, addrSize, nullptr, ip, &ipSize);
	senderIp = ip;

	int pos = senderIp.find(':');
	string port = senderIp.substr(pos);
	senderPort = atoi(port.substr(1).c_str());
	senderIp = senderIp.substr(0, senderIp.find(':'));

	return msgSize;
}

bool NetworkHelper::CanReceiveUDPPacket()
{
	fd_set fdSet;
	FD_ZERO(&fdSet);
	FD_SET(udpSocket, &fdSet);
	TIMEVAL t = {0, 0};
	select(0, &fdSet, nullptr, nullptr, &t);

	return FD_ISSET(udpSocket, &fdSet);
}