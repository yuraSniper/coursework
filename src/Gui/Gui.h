#pragma once

#include <common.h>
#include <Gui\GuiContainer.h>

class Gui : public GuiContainer
{
public:
	Gui(int x = 0, int y = 0, int width = 0, int height = 0) : GuiContainer(x, y, width, height)
	{
		showHorizontalScrollBar = false;
		showVerticalScrollBar = false;
	}

	virtual void Init() = 0;
};