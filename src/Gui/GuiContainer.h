#pragma once

#include <common.h>
#include <Gui\GuiComponent.h>

class GuiScrollBar;

class GuiContainer : public GuiComponent, public enable_shared_from_this<GuiContainer>
{
	void UpdateScrollBars();
protected:
	vector<shared_ptr<GuiComponent>> components;

	virtual void DrawImplPost(ivec2 mouse, GuiRenderBuffer & buffer)
	{
	}

	virtual bool CheckInputImplPost(shared_ptr<Input> input, bool inputEaten)
	{
		return false;
	}

	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInputImpl(shared_ptr<Input> input);

	virtual void DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer)
	{
	}

	virtual void CheckInputImplPre(shared_ptr<Input> input)
	{
	}

	bool prevMouseDown = false;
	shared_ptr<GuiScrollBar> vertical, horizontal;
	bool releaseFocus = false;
public:
	shared_ptr<GuiComponent> focused;
	bool showHorizontalScrollBar = true;
	bool showVerticalScrollBar = true;
	GuiArea interiorBoundingBox;
	GuiArea visibleContent;
	int color = 0;

	GuiContainer(int x = 0, int y = 0, int width = 0, int height = 0);

	virtual void Draw(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInput(shared_ptr<Input> input);
	virtual bool CanHaveFocus();

	void AddComponent(shared_ptr<GuiComponent> component, int index = -1);
	void RemoveComponent(shared_ptr<GuiComponent> component);
	void RemoveComponent(int index);
	shared_ptr<GuiComponent> GetComponent(int index);
	int GetComponentCount();

	shared_ptr<GuiComponent> GetFocusedComponent();
};