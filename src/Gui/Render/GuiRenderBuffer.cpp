#include "GuiRenderBuffer.h"

#include <Gui\GuiArea.h>
#include <Main\Main.h>
#include <Render\Resource\Font.h>
#include <Render\Resource\ResourceManager.h>
#include <Render\Shader\ShaderProgram.h>

vec2 unmix(vec2 a, vec2 b, vec2 x)
{
	return (x - a) / (b - a);
}

float unmix(float a, float b, float x)
{
	return (x - a) / (b - a);
}

float fade(float t)
{
	//return t * t * (3 - 2 * t);
	return t * t * t * (10 + (-15 + 6 * t) * t);
	//return t;
}

GuiRenderBuffer::GuiRenderBuffer(shared_ptr<ShaderProgram> guiShader, Font * guiFont)
{
	font = guiFont;
	shader = guiShader;

	shader->Bind();

	glUniform1i(shader->GetUniformLocation("tex"), 1);
	uniformMVP = shader->GetUniformLocation("mvp");

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex), (void *) offsetof(Vertex, x));
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, true, sizeof(Vertex), (void *) offsetof(Vertex, color));
	glVertexAttribPointer(2, 2, GL_FLOAT, false, sizeof(Vertex), (void *) offsetof(Vertex, s));
	glVertexAttribPointer(3, 1, GL_FLOAT, false, sizeof(Vertex), (void *) offsetof(Vertex, alphaThreshold));
	glVertexAttribIPointer(4, 1, GL_UNSIGNED_SHORT, sizeof(Vertex), (void *) offsetof(Vertex, c));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
}

void GuiRenderBuffer::AddPoint(int x, int y, int color)
{
	if (!enablePoints)
		return;

	Vertex v = {x, y, zOffset, color, 0, 0, 0, 0};

	if (cullingEnabled)
	{
		GuiArea parent = GuiArea::GetClampedParent();
		if (!parent.HitTestRel(ivec2(v.x, v.y)))
			return;
	}
	v.x += GuiArea::GetAbsOffset().x;
	v.y += GuiArea::GetAbsOffset().y;

	pointBuffer.push_back(v);
}

void GuiRenderBuffer::AddLine(int x1, int y1, int x2, int y2, int color)
{
	if (!enableLines)
		return;

	vec2 points[2] = {vec2(x1, y1), vec2(x2, y2)};
	vec2 result[2];

	if (cullingEnabled)
	{
		GuiArea parent = GuiArea::GetClampedParent();

		for (int i = 0; i < 2; i++)
		{
			if (!parent.HitTestRel(points[i]))
			{
				float tx = 0;
				float ty = 0;

				vec2 t1 = unmix(points[i].xx, points[1 - i].xx, vec2(0, parent.width - 1));
				tx = glm::min(t1.x, t1.y);
				if (tx < 0 || tx > 1)
					tx = 0;

				vec2 t2 = unmix(points[i].yy, points[1 - i].yy, vec2(0, parent.height - 1));
				ty = glm::min(t2.x, t2.y);
				if (ty < 0 || ty > 1)
					ty = 0;

				float t = glm::max(tx, ty);

				result[i] = vec2(mix(points[i].x, points[1 - i].x, t), mix(points[i].y, points[1 - i].y, t));
				if (!parent.HitTestRel(result[i]))
					return;
			}
			else
				result[i] = points[i];
		}
	}

	Vertex v1 = {result[0].x, result[0].y, zOffset, color, 0, 0, 0, 0};
	Vertex v2 = {result[1].x, result[1].y, zOffset, color, 0, 0, 0, 0};

	v1.x += GuiArea::GetAbsOffset().x;
	v1.y += GuiArea::GetAbsOffset().y;
	v2.x += GuiArea::GetAbsOffset().x;
	v2.y += GuiArea::GetAbsOffset().y;

	lineBuffer.push_back(v1);
	lineBuffer.push_back(v2);
}

void GuiRenderBuffer::AddRect(int x1, int y1, int x2, int y2, int color)
{
	AddRectSized(x1, y1, x2 - x1, y2 - y1, color);
}

void GuiRenderBuffer::AddRectSized(int x, int y, int width, int height, int color)
{
	if (!enableRects)
		return;

	GuiArea rect = GuiArea(x, y, width, height);

	if (cullingEnabled)
		rect = rect.IntersectWithParent();

	if (rect.width == 0 || rect.height == 0)
		return;

	rect = rect.MakeAbsolute();

	triangleBuffer.push_back({(float)rect.x, (float)rect.y, zOffset, color, 0, 0, 0, 0});
	triangleBuffer.push_back({(float)rect.x, (float)rect.y + rect.height, zOffset, color, 0, 0, 0, 0});
	triangleBuffer.push_back({(float)rect.x + rect.width, (float)rect.y, zOffset, color, 0, 0, 0, 0});
	triangleBuffer.push_back({(float)rect.x + rect.width, (float)rect.y, zOffset, color, 0, 0, 0, 0});
	triangleBuffer.push_back({(float)rect.x, (float)rect.y + rect.height, zOffset, color, 0, 0, 0, 0});
	triangleBuffer.push_back({(float)rect.x + rect.width, (float)rect.y + rect.height, zOffset, color, 0, 0, 0, 0});
}

void GuiRenderBuffer::AddRectSized(GuiArea & rect, int color)
{
	AddRectSized(rect.x, rect.y, rect.width, rect.height, color);
}

void GuiRenderBuffer::AddTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int color)
{
	if (!enableTriangles)
		return;

	ivec2 offset = GuiArea::GetAbsOffset();

	triangleBuffer.push_back({(float) x1 + offset.x, (float) y1 + offset.y, zOffset, color, 0, 0, 0, 0});
	triangleBuffer.push_back({(float) x2 + offset.x, (float) y2 + offset.y, zOffset, color, 0, 0, 0, 0});
	triangleBuffer.push_back({(float) x3 + offset.x, (float) y3 + offset.y, zOffset, color, 0, 0, 0, 0});
}

void GuiRenderBuffer::AddSpline(int x1, int y1, int x2, int y2, int color)
{
	//AddLine(x1, y1, x2, y2, color);
	//return;

	vector<ivec2> points;
	int count = glm::max(1, abs(x2 - x1));
	for (int t = 0; t <= count; t++)
	{
		float tt = (float) t / count;
		points.push_back(ivec2(glm::mix(x1, x2, fade(tt)), glm::mix(y1, y2, tt)));
	}
	
	for (int i = 0; i < points.size() - 1; i++)
	{
		ivec2 p1 = points[i];
		ivec2 p2 = points[i + 1];
		AddLine(p1.x, p1.y, p2.x, p2.y, color);
	}
}

void GuiRenderBuffer::AddString(int x, int y, float size, int color, u16string str)
{
	if (!enableStrings)
		return;

	GuiArea parent = GuiArea::GetClampedParent();
	parent = parent.MakeAbsolute();

	x += GuiArea::GetAbsOffset().x;
	y += GuiArea::GetAbsOffset().y;

	float width = GetTextWidth(str.length(), size);
	float remainderWidth = x + width - parent.x - parent.width;

	if (remainderWidth > 0)
	{
		int pos = floor((width - remainderWidth) * 32.0f / (18.0f * size));
		if (pos > 0)
		{
			str = str.substr(0, pos - 1);
			str.append(u"�");
		}
		else
			return;
	}

	float alphaThreshold;
	if (size < 32)
		alphaThreshold = mix(0.5f, 0.4f, (size - 12.0f) / 20.0f);
	else if (size < 64)
		alphaThreshold = mix(0.4f, 0.2f, (size - 32.0f) / 32.0f);
	else if (size < 128)
		alphaThreshold = mix(0.2f, 0.1f, (size - 64.0f) / 64.0f);
	else if (size < 256)
		alphaThreshold = mix(0.1f, 1 / 16.0f, (size - 128.0f) / 128.0f);
	else
		alphaThreshold = glm::fastInverseSqrt(size);

	y -= textBaseLinePos * size;

	vec2 minCorner = vec2(parent.x, parent.y);
	vec2 maxCorner = minCorner + vec2(parent.width - 1, parent.height - 1);

	float offset = 0;
	for (int i = 0; i < str.length(); i++)
	{
		if (str[i] != 32)
		{
			if (!font->pages[str[i] >> 8])
				ResourceManager::GetInstance().LoadFontPage(*font, str[i] >> 8);
			
			float position = offset;
			float tmp = 18.0f / 32.0f - font->glyphWidth[str[i]] / 32.0f;
			if (tmp > 0)
				position += tmp * size / 2.0f;

			vec2 p1 = vec2(x + position, y);
			vec2 p2 = p1 + vec2(font->glyphWidth[str[i]] * size / 32.0f, size);

			vec2 t1 = vec2((1 - font->glyphWidth[str[i]] / 32.0f) * 0.5f, 0);
			vec2 t2 = vec2((1 + font->glyphWidth[str[i]] / 32.0f) * 0.5f, 1);

			if (cullingEnabled)
			{
				vec2 newp1 = clamp(p1, minCorner, maxCorner);
				vec2 newp2 = clamp(p2, minCorner, maxCorner);

				vec2 newt1 = mix(t1, t2, unmix(p1, p2, newp1));
				vec2 newt2 = mix(t1, t2, unmix(p1, p2, newp2));

				p1 = newp1;
				p2 = newp2;
				t1 = newt1;
				t2 = newt2;
			}
			
			triangleBuffer.push_back({p1.x, p1.y, zOffset, color, t1.x, t1.y, alphaThreshold, str[i]});
			triangleBuffer.push_back({p1.x, p2.y, zOffset, color, t1.x, t2.y, alphaThreshold, str[i]});
			triangleBuffer.push_back({p2.x, p1.y, zOffset, color, t2.x, t1.y, alphaThreshold, str[i]});
			triangleBuffer.push_back({p2.x, p1.y, zOffset, color, t2.x, t1.y, alphaThreshold, str[i]});
			triangleBuffer.push_back({p1.x, p2.y, zOffset, color, t1.x, t2.y, alphaThreshold, str[i]});
			triangleBuffer.push_back({p2.x, p2.y, zOffset, color, t2.x, t2.y, alphaThreshold, str[i]});
		}
		offset += 18.0f * size / 32.0f;
	}
}

void GuiRenderBuffer::IncrementZoffset(int amount)
{
	zOffset += amount;
}

void GuiRenderBuffer::DecrementZoffset(int amount)
{
	zOffset -= amount;
}

int GuiRenderBuffer::SetZoffset(int offset)
{
	int tmp = zOffset;
	zOffset = offset;
	return tmp;
}

void GuiRenderBuffer::Clear()
{
	triangleBuffer.clear();
	lineBuffer.clear();
	pointBuffer.clear();
}

void GuiRenderBuffer::Draw()
{
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glPushAttrib(GL_COLOR_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0);

	shader->Bind();
	glUniformMatrix4fv(uniformMVP, 1, false, value_ptr(Main::GetInstance().GetMVP()));

	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * triangleBuffer.size(), triangleBuffer.data(), GL_STREAM_DRAW);
	glDrawArrays(GL_TRIANGLES, 0, triangleBuffer.size());

	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * lineBuffer.size(), lineBuffer.data(), GL_STREAM_DRAW);
	glDrawArrays(GL_LINES, 0, lineBuffer.size());

	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * pointBuffer.size(), pointBuffer.data(), GL_STREAM_DRAW);
	glDrawArrays(GL_POINTS, 0, pointBuffer.size());

	glPopAttrib();

	glBindVertexArray(0);
}

float GuiRenderBuffer::GetTextBaselinePos(float size)
{
	return 190 * size / 255.0f;
}

float GuiRenderBuffer::GetTextWidth(u16string text, float size)
{
	return GetTextWidth(text.length(), size);
}

float GuiRenderBuffer::GetTextWidth(int textLength, float size)
{
	return size * 18.0f * textLength / 32.0f;
}

void GuiRenderBuffer::AddRectOutline(int x, int y, int width, int height, int color)
{
	//AddRectSized(x, y, width, height, 0xFF007FFF);
	//AddRectSized(10, 10, 50, 50, 0xFF00FF00);
	//AddLine(10, 10, 70, 70, 0xFFFF0000);
	AddLine(x, y, x, y + height, color);
	AddLine(x, y + height, x + width, y + height, color);
	AddLine(x + width, y + height, x + width, y, color);
	AddLine(x + width, y, x, y, color);
}