#pragma once

#include <common.h>

class Font;
class ShaderProgram;
class GuiArea;

class GuiRenderBuffer
{
	struct Vertex
	{
		float x, y, z;
		int color;
		float s, t;
		float alphaThreshold;
		char16_t c;
	};

	vector<Vertex> triangleBuffer;
	vector<Vertex> lineBuffer;
	vector<Vertex> pointBuffer;

	unsigned int vbo = 0, vao = 0;

	shared_ptr<ShaderProgram> shader;
	Font * font;
	unsigned int uniformMVP;
	float zOffset = 0;

	float textBaseLinePos = 190.0f / 255.0f;

public:
	
	GuiRenderBuffer(shared_ptr<ShaderProgram> guiShader, Font * guiFont);

	bool cullingEnabled = true;
	bool enablePoints = true;
	bool enableLines = true;
	bool enableRects = true;
	bool enableTriangles = true;
	bool enableStrings = true;

	void AddPoint(int x, int y, int color);
	void AddLine(int x1, int y1, int x2, int y2, int color);
	void AddRect(int x1, int y1, int x2, int y2, int color);
	void AddRectSized(int x, int y, int width, int height, int color);
	void AddRectSized(GuiArea & rect, int color);
	void AddRectOutline(int x, int y, int width, int height, int color);
	void AddTriangle(int x1, int y1, int x2, int y2, int x3, int y3, int color);
	void AddSpline(int x1, int y1, int x2, int y2, int color);
	void AddString(int x, int y, float size, int color, u16string str);

	void IncrementZoffset(int amount = 1);
	void DecrementZoffset(int amount = 1);
	int SetZoffset(int offset);

	void Clear();
	void Draw();

	static float GetTextBaselinePos(float size = 12);
	static float GetTextWidth(u16string text, float size);
	static float GetTextWidth(int textLength, float size);
};