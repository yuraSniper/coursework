#include "GuiContainer.h"

#include <Gui\Render\GuiRenderBuffer.h>
#include <Gui\Components\GuiScrollBar.h>
#include <Window\Input.h>

void GuiContainer::UpdateScrollBars()
{
	visibleContent = boundingBox;
	visibleContent.x = visibleContent.y = 0;

	if (showVerticalScrollBar)
	{
		visibleContent.width -= 16;
		horizontal->boundingBox = GuiArea(0, boundingBox.height - 16, boundingBox.width - 16, 16);
	}
	else
		horizontal->boundingBox = GuiArea(0, boundingBox.height - 16, boundingBox.width, 16);

	if (showHorizontalScrollBar)
	{
		visibleContent.height -= 16;
		vertical->boundingBox = GuiArea(boundingBox.width - 16, 0, 16, boundingBox.height - 16);
	}
	else
		vertical->boundingBox = GuiArea(boundingBox.width - 16, 0, 16, boundingBox.height);
}

void GuiContainer::DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer)
{
	for (int i = 0; i < components.size(); i++)
	{
		if (components[i] == nullptr)
		{
			components.erase(components.begin() + i);
			i--;
			continue;
		}
		components[i]->Draw(mouse, buffer);
	}
}

bool GuiContainer::CheckInputImpl(shared_ptr<Input> input)
{
	ivec2 mousePos = input->GetMousePos();
	bool mouseDown = input->GetMouseButton(LEFT);
	bool eatInput = false;
	bool b = false;

	for (int i = components.size() - 1; i >= 0; i--)
	{
		if (components[i] != nullptr)
		{
			shared_ptr<GuiComponent> component = components[i];

			if (!b && !prevMouseDown && mouseDown && component->boundingBox.IntersectWithParent().HitTestAbs(mousePos))
			{
				if (component->CanHaveFocus())
				{
					focused = component;
					b = true;
				}
				else if (releaseFocus)
					focused = nullptr;
			}

			if (component->CheckInput(input))
			{
				eatInput = true;
				b = true;
				break;
			}
		}
	}

	for (int i = 0; i < components.size(); i++)
		if (components[i] == nullptr)
		{
			components.erase(components.begin() + i);
			i--;
			continue;
		}

	if (!prevMouseDown && mouseDown && !b && releaseFocus)
		focused = nullptr;

	prevMouseDown = mouseDown;

	return eatInput;
}

GuiContainer::GuiContainer(int x, int y, int width, int height) : GuiComponent(x, y, width, height)
{
	interiorBoundingBox = GuiArea(0, 0, boundingBox.width, boundingBox.height);
	
	vertical = make_shared<GuiScrollBar>();
	horizontal = make_shared<GuiScrollBar>(false);
}

void GuiContainer::Draw(ivec2 mouse, GuiRenderBuffer & buffer)
{
	clampedBoundingBox = boundingBox;
	clampedBoundingBox.MakeParent();

	vertical->parent = shared_from_this();
	horizontal->parent = shared_from_this();

	UpdateScrollBars();
	
	DrawImplPre(mouse, buffer);

	buffer.AddRectSized(0, 0, boundingBox.width, boundingBox.height, color);

	if (showVerticalScrollBar)
		vertical->Draw(mouse, buffer);
	if (showHorizontalScrollBar)
		horizontal->Draw(mouse, buffer);

	if (showHorizontalScrollBar && showVerticalScrollBar)
		buffer.AddRectSized(boundingBox.width - 16, boundingBox.height - 16, 16, 16, 0xFF252525);

	visibleContent.MakeParent();

	GuiArea tmp = interiorBoundingBox;
	tmp.MakeParentUnclamped();

	DrawImpl(mouse, buffer);

	DrawImplPost(mouse, buffer);
	tmp.UnmakeParent();
	visibleContent.UnmakeParent();
	clampedBoundingBox.UnmakeParent();
}

bool GuiContainer::CheckInput(shared_ptr<Input> input)
{
	clampedBoundingBox = boundingBox;
	clampedBoundingBox.MakeParent();

	CheckInputImplPre(input);
	
	UpdateScrollBars();

	if (showVerticalScrollBar)
		vertical->CheckInput(input);
	if (showHorizontalScrollBar)
		horizontal->CheckInput(input);

	visibleContent.MakeParent();

	GuiArea tmp = interiorBoundingBox;
	tmp.MakeParent();

	bool eatInput = CheckInputImpl(input);

	eatInput |= CheckInputImplPost(input, eatInput);
	tmp.UnmakeParent();
	visibleContent.UnmakeParent();
	clampedBoundingBox.UnmakeParent();

	return eatInput;
}

bool GuiContainer::CanHaveFocus()
{
	return true;
}

void GuiContainer::AddComponent(shared_ptr<GuiComponent> component, int index)
{
	if (index == -1)
		components.push_back(component);
	else
		components.insert(components.begin() + index, component);

	component->parent = shared_from_this();
}

void GuiContainer::RemoveComponent(shared_ptr<GuiComponent> component)
{
	auto pos = find(components.begin(), components.end(), component);
	if (pos != components.end())
	{
		component->parent.reset();
		components.erase(pos);
	}
}

void GuiContainer::RemoveComponent(int index)
{
	components[index] = nullptr;
	//components.erase(components.begin() + index);
}

shared_ptr<GuiComponent> GuiContainer::GetComponent(int index)
{
	if (index >= 0 && index < components.size())
		return components.at(index);

	return nullptr;
}

int GuiContainer::GetComponentCount()
{
	return components.size();
}

shared_ptr<GuiComponent> GuiContainer::GetFocusedComponent()
{
	if (parent.expired() || parent.lock()->GetFocusedComponent() == shared_from_this())
		return focused;

	return nullptr;
}