#pragma once

#include <common.h>
#include <glm.h>

class GuiArea
{
	static ivec2 absoluteOffset;
public:
	int x, y, width, height;

	GuiArea(int x = 0, int y = 0, int width = 0, int height = 0);

	void MakeParent();
	void MakeParentUnclamped();
	void UnmakeParent();
	void PushParent();
	void PopParent();

	static GuiArea GetParent();
	static GuiArea GetClampedParent();
	static ivec2 GetAbsOffset();

	GuiArea Intersect(GuiArea with);
	GuiArea ClampToParent();
	GuiArea IntersectWithParent();
	GuiArea MakeAbsolute();
	GuiArea MakeRelative(GuiArea to);

	ivec2 ClampPoint(ivec2 point);

	bool Contains(GuiArea area);
	bool Equals(GuiArea area);
	bool HitTestRel(ivec2 position, bool clamp = true);
	bool HitTestAbs(ivec2 position, bool clamp = true);
};