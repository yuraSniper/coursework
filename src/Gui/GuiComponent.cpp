#include "GuiComponent.h"

#include <Gui\GuiContainer.h>
#include <Window\Input.h>

GuiComponent::GuiComponent(int x, int y, int width, int height)
{
	boundingBox = GuiArea(x, y, width, height);
	clampedBoundingBox = boundingBox;
}

void GuiComponent::Draw(ivec2 mouse, GuiRenderBuffer & buffer)
{
	clampedBoundingBox = boundingBox;
	clampedBoundingBox.MakeParent();

	DrawImpl(mouse, buffer);

	clampedBoundingBox.UnmakeParent();
}

bool GuiComponent::CheckInput(shared_ptr<Input> input)
{
	clampedBoundingBox = boundingBox;
	clampedBoundingBox.MakeParent();

	bool eatinput = CheckInputImpl(input);

	clampedBoundingBox.UnmakeParent();

	return eatinput;
}

bool GuiComponent::IsFocused()
{
	if (parent.expired())
		return false;

	return this == parent.lock()->GetFocusedComponent().get();
}

bool GuiComponent::CanHaveFocus()
{
	return false;
}