#pragma once

#include <common.h>
#include <Gui\GuiArea.h>

class Input;
class GuiRenderBuffer;
class GuiContainer;

class GuiComponent
{
protected:
	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer) = 0;
	virtual bool CheckInputImpl(shared_ptr<Input> input) = 0;
public:
	weak_ptr<GuiContainer> parent;
	GuiArea boundingBox;
	GuiArea clampedBoundingBox;
	
	GuiComponent(int x = 0, int y = 0, int width = 0, int height = 0);

	virtual void Draw(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInput(shared_ptr<Input> input);
	bool IsFocused();
	virtual bool CanHaveFocus();
};