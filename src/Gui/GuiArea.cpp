#include "GuiArea.h"

stack<GuiArea> areaStack;
stack<GuiArea> clampedAreaStack;
ivec2 GuiArea::absoluteOffset;

GuiArea::GuiArea(int x, int y, int width, int height)
{
	if (width < 0)
	{
		width = -width;
		x -= width;
	}

	if (height < 0)
	{
		height = -height;
		y -= height;
	}

	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
}

void GuiArea::MakeParent()
{
	if (areaStack.empty())
	{
		areaStack.push(*this);
		absoluteOffset += ivec2(x, y);
		clampedAreaStack.push(*this);
		return;
	}
	GuiArea clamped = ClampToParent();

	areaStack.push(*this);
	clampedAreaStack.push(clamped);
	absoluteOffset += ivec2(x, y);
	*this = clamped;
}

void GuiArea::MakeParentUnclamped()
{
	if (areaStack.empty())
	{
		areaStack.push(*this);
		absoluteOffset += ivec2(x, y);
		clampedAreaStack.push(*this);
		return;
	}
	GuiArea clamped = ClampToParent();

	areaStack.push(*this);
	clampedAreaStack.push(clamped);
	absoluteOffset += ivec2(x, y);
	x = y = 0;
}

void GuiArea::UnmakeParent()
{
	*this = GetParent();
	absoluteOffset -= ivec2(x, y);
	areaStack.pop();
	clampedAreaStack.pop();
}

void GuiArea::PushParent()
{
	if (areaStack.empty())
	{
		areaStack.push(*this);
		absoluteOffset += ivec2(x, y);
		clampedAreaStack.push(*this);
		return;
	}

	areaStack.push(*this);
	absoluteOffset += ivec2(x, y);
	x = y = 0;
	clampedAreaStack.push(*this);
}

void GuiArea::PopParent()
{
	*this = GetParent();
	absoluteOffset -= ivec2(x, y);
	areaStack.pop();
	clampedAreaStack.pop();
}

GuiArea GuiArea::GetParent()
{
	return areaStack.top();
}

GuiArea GuiArea::GetClampedParent()
{
	return clampedAreaStack.top();
}

ivec2 GuiArea::GetAbsOffset()
{
	return absoluteOffset;
}

GuiArea GuiArea::Intersect(GuiArea with)
{
	ivec2 v1 = {x, y};
	ivec2 v2 = v1 + ivec2(width, height);
	
	ivec2 c1 = {with.x, with.y};
	ivec2 c2 = c1 + ivec2(with.width, with.height);

	v1 = clamp(v1, c1, c2);
	v2 = clamp(v2, c1, c2);

	return GuiArea(v1.x, v1.y, v2.x - v1.x, v2.y - v1.y);
}

GuiArea GuiArea::ClampToParent()
{
	GuiArea result = Intersect(GetClampedParent());
	result.x -= x;
	result.y -= y;

	return result;
}

GuiArea GuiArea::IntersectWithParent()
{
	GuiArea parent = GetClampedParent();

	return Intersect(parent);
}

GuiArea GuiArea::MakeAbsolute()
{
	return GuiArea(x + absoluteOffset.x, y + absoluteOffset.y, width, height);
}

GuiArea GuiArea::MakeRelative(GuiArea to)
{
	return GuiArea(x - to.x, y - to.y, width, height);
}

glm::ivec2 GuiArea::ClampPoint(ivec2 point)
{
	GuiArea clamped = ClampToParent();
	ivec2 c1 = ivec2(clamped.x, clamped.y);
	ivec2 c2 = c1 + ivec2(clamped.width - 1, clamped.height - 1);

	return clamp(point, c1, c2);
}

bool GuiArea::Contains(GuiArea area)
{
	GuiArea absolute = MakeAbsolute();
	area = area.MakeAbsolute();

	return area.Equals(area.Intersect(absolute));
}

bool GuiArea::Equals(GuiArea area)
{
	return x == area.x && y == area.y && width == area.width && height == area.height;
}

bool GuiArea::HitTestRel(ivec2 position, bool clamp)
{
	GuiArea clamped = *this;
	position -= ivec2(x, y);
	clamped.x = clamped.y = 0;

	if (clamp)
		clamped = ClampToParent();
	return position.x >= clamped.x && position.y >= clamped.y && position.x <= clamped.x + clamped.width && position.y <= clamped.y + clamped.height;
}

bool GuiArea::HitTestAbs(ivec2 position, bool clamp)
{
	return HitTestRel(position - absoluteOffset, clamp);
}