#include "GuiComponentList.h"

void GuiComponentList::UpdateContent()
{
	interiorBoundingBox.width = 0;
	interiorBoundingBox.height = 0;

	for (shared_ptr<GuiComponent> component : components)
	{
		component->boundingBox.y = interiorBoundingBox.height;
		component->boundingBox.x = 0;

		interiorBoundingBox.height += component->boundingBox.height;
		interiorBoundingBox.width = glm::max(interiorBoundingBox.width, component->boundingBox.width);
	}
}

GuiComponentList::GuiComponentList(int x, int y, int width, int height) : GuiContainer(x, y, width, height)
{
	color = 0xFF202020;
}

void GuiComponentList::DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer)
{
	UpdateContent();
}