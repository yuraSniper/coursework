#include "GuiScrollBar.h"

#include <Gui\GuiArea.h>
#include <Gui\Render\GuiRenderBuffer.h>
#include <Gui\GuiContainer.h>
#include <Window\Input.h>

void GuiScrollBar::DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer)
{
	shared_ptr<GuiContainer> p = parent.lock();
	GuiArea bar = CalculateBar(p);

	bool hovered = bar.HitTestAbs(mouse);

	buffer.AddRectSized(0, 0, boundingBox.width, boundingBox.height, 0xFF252525);
	buffer.AddRectSized(bar.x, bar.y, bar.width, bar.height, dragging? 0xFFAAAAAA : hovered? 0xFF7F7F7F : 0xFF505050);
}

bool GuiScrollBar::CheckInputImpl(shared_ptr<Input> input)
{
	bool mouseDown = input->GetMouseButton(LEFT);
	ivec2 mousePos = input->GetMousePos();

	shared_ptr<GuiContainer> p = parent.lock();
	GuiArea bar = CalculateBar(p);

	if (bar.HitTestAbs(mousePos) && mouseDown && !mouseWasDown)
	{
		dragging = true;
		prevMousePos = mousePos;
	}

	if (!mouseDown)
		dragging = false;

	if (dragging)
	{
		ivec2 delta = mousePos - prevMousePos;
		if (vertical)
			p->interiorBoundingBox.y = glm::clamp((int) (p->interiorBoundingBox.y - delta.y / coef), -glm::max(0, p->interiorBoundingBox.height - p->visibleContent.height), 0);
		else
			p->interiorBoundingBox.x = glm::clamp((int) (p->interiorBoundingBox.x - delta.x / coef), -glm::max(0, p->interiorBoundingBox.width - p->visibleContent.width), 0);

		prevMousePos = mousePos;
	}

	mouseWasDown = mouseDown;

	return false;
}

GuiArea GuiScrollBar::CalculateBar(shared_ptr<GuiContainer> p)
{
	GuiArea bar;
	if (vertical)
	{
		coef = p->visibleContent.height / (float) p->interiorBoundingBox.height;
		int barLength = boundingBox.height * coef;
		bar = GuiArea(boundingBox.width / 4, 2 - p->interiorBoundingBox.y * coef, boundingBox.width / 2, barLength - 4);
	}
	else
	{
		coef = p->visibleContent.width / (float) p->interiorBoundingBox.width;
		int barLength = boundingBox.width * coef;
		bar = GuiArea(2 - p->interiorBoundingBox.x * coef, boundingBox.height / 4, barLength - 4, boundingBox.height / 2);
	}
	return bar;
}

GuiScrollBar::GuiScrollBar(bool vertical, int x, int y, int width, int height) : GuiComponent(x, y, width, height)
{
	this->vertical = vertical;
}