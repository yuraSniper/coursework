#pragma once

#include <common.h>
#include <Gui\Components\SubComponents\GuiMenuItem.h>

class Input;
class GuiRenderBuffer;

class GuiMenu : public GuiMenuItem, public enable_shared_from_this<GuiMenu>
{
	void CalculateSize();
	void AddItem(shared_ptr<GuiMenuItem> item, int pos = -1);

	vector<shared_ptr<GuiMenuItem>> items;
	int nextPos = 0;
	bool prevMouseState = false;
	bool isDown = false;

public:
	shared_ptr<GuiMenu> openedSubMenu = nullptr;
	
	GuiMenu(u16string name);

	virtual void Draw(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInput(shared_ptr<Input> input);

	void AddItem(u16string name, int pos = -1);
	void AddMenu(shared_ptr<GuiMenu> item, int pos = -1);
	void RemoveItem(int pos = -1);
	void CloseSubmenus();
};