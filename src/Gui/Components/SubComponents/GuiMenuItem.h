#pragma once

#include <common.h>
#include <Gui\GuiArea.h>
#include <Util\Delegate.h>

class GuiMenu;

class GuiMenuItem
{
protected:
	virtual void v()
	{
	}
public:
	bool isMenu = false;
	u16string name = u"";
	GuiArea boundingBox = GuiArea();
	
	weak_ptr<GuiMenu> parent;
	Delegate<GuiMenuItem *, int> onAction;
};