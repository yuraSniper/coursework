#include "GuiMenu.h"

#include <Gui\Render\GuiRenderBuffer.h>
#include <Window\Input.h>

void GuiMenu::CalculateSize()
{
	boundingBox.width = 0;
	boundingBox.height = 20 * items.size();

	for (int i = 0; i < items.size(); i++)
	{
		boundingBox.width = glm::max(boundingBox.width, 16 + (int)floor(GuiRenderBuffer::GetTextWidth(items[i]->name.length(), 14) + 0.5f));
	}
}

void GuiMenu::Draw(ivec2 mouse, GuiRenderBuffer & buffer)
{
	int prevZ = buffer.SetZoffset(1000);

	buffer.AddRectSized(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, 0xFF454545);

	for (int i = 0; i < items.size(); i++)
	{
		shared_ptr<GuiMenuItem> item = items[i];

		if (GuiArea(boundingBox.x, boundingBox.y + i * 20, boundingBox.width, 20).HitTestAbs(mouse))
			buffer.AddRectSized(boundingBox.x, boundingBox.y + i * 20, boundingBox.width, 20, 0xFF555555);

		buffer.AddString(8 + boundingBox.x, buffer.GetTextBaselinePos(20) + boundingBox.y + i * 20, 14, 0xFFFFFFFF, item->name);

		if (item->isMenu)
			buffer.AddTriangle(boundingBox.x + boundingBox.width - 6, 6 + boundingBox.y + i * 20, boundingBox.x + boundingBox.width - 6, 13 + boundingBox.y + i * 20, boundingBox.x + boundingBox.width - 2, 9 + boundingBox.y + i * 20, 0xFFFFFFFF);
	}

	if (openedSubMenu != nullptr)
		openedSubMenu->Draw(mouse, buffer);

	buffer.SetZoffset(prevZ);
}

bool GuiMenu::CheckInput(shared_ptr<Input> input)
{
	bool mouseDown = input->GetMouseButton(LEFT);
	ivec2 mouse = input->GetMousePos();
	bool keepOpen = true;

	if (boundingBox.HitTestAbs(mouse))
	{
		int pos = glm::clamp<int>((mouse.y - boundingBox.y - GuiArea::GetAbsOffset().y) / 20, 0, items.size() - 1);
		shared_ptr<GuiMenuItem> item = items[pos];

		if (item->isMenu)
		{
			item->boundingBox.x = boundingBox.x + boundingBox.width;
			item->boundingBox.y = boundingBox.y + GuiArea::GetAbsOffset().y + pos * 20;

			shared_ptr<GuiMenu> menu = dynamic_pointer_cast<GuiMenu>(item);
			if (menu != openedSubMenu)
				CloseSubmenus();
			openedSubMenu = menu;
		}
		else if (!mouseDown && isDown)
		{
			if (openedSubMenu != nullptr)
				openedSubMenu->CloseSubmenus();

			keepOpen = false;
		
			onAction(items[pos].get(), pos);
		}
		else
			CloseSubmenus();

		if (mouseDown && !prevMouseState)
			isDown = true;
	}
	else if (mouseDown && !prevMouseState)
		keepOpen = false;
	
	if (openedSubMenu != nullptr)
		keepOpen = openedSubMenu->CheckInput(input);

	if (!mouseDown)
		isDown = false;

	prevMouseState = mouseDown;
	return keepOpen;
}

GuiMenu::GuiMenu(u16string name)
{
	this->name = name;
	isMenu = true;
}

void GuiMenu::AddItem(u16string name, int pos)
{
	shared_ptr<GuiMenuItem> item = make_shared<GuiMenuItem>();
	item->name = name;
	AddItem(item, pos);
}

void GuiMenu::AddMenu(shared_ptr<GuiMenu> item, int pos)
{
	AddItem(item, pos);
}

void GuiMenu::AddItem(shared_ptr<GuiMenuItem> item, int pos)
{
	if (pos == -1)
		pos = items.size();
	else if ((unsigned) pos > items.size())
		return;

	item->parent = shared_from_this();
	items.insert(items.begin() + pos, item);
	
	boundingBox.width = glm::max(boundingBox.width, (int)(16 + GuiRenderBuffer::GetTextWidth(item->name.length(), 14)));
	boundingBox.height += 20;
}

void GuiMenu::RemoveItem(int pos)
{
	if (pos == -1)
		pos = items.size() - 1;
	else if ((unsigned) pos > items.size())
		return;

	items.erase(items.begin() + pos);
	CalculateSize();
}

void GuiMenu::CloseSubmenus()
{
	if (openedSubMenu != nullptr)
		openedSubMenu->CloseSubmenus();
	openedSubMenu = nullptr;
}