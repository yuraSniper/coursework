#pragma once

#include <common.h>
#include <Gui\GuiComponent.h>

class GuiScrollBar : public GuiComponent
{
	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInputImpl(shared_ptr<Input> input);

	GuiArea CalculateBar(shared_ptr<GuiContainer> p);

	bool mouseWasDown = false;
	bool dragging = false;
	bool vertical = false;
	float coef = 1;
	ivec2 prevMousePos;
public:
	GuiScrollBar(bool vertical = true, int x = 0, int y = 0, int width = 0, int height = 0);
};