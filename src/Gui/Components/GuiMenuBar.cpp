#include "GuiMenuBar.h"

#include <Gui\Render\GuiRenderBuffer.h>
#include <Gui\Components\SubComponents\GuiMenu.h>
#include <Gui\GuiContainer.h>
#include <Window\Input.h>

void GuiMenuBar::DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer)
{
	buffer.AddRectSized(0, 0, boundingBox.width, 16, 0xFF505050);

	int pos = 0;
	int openedPos = 0;

	for (int i = 0; i < menus.size(); i++)
	{
		int width = 16 + buffer.GetTextWidth(menus[i]->name.length(), 14);

		if (openedMenu != nullptr && openedMenu.get() == menus[i].get())
			openedPos = pos;

		if (GuiArea(pos, 0, width, 16).HitTestAbs(mouse))
			buffer.AddRectSized(pos, 0, width, 16, 0xFF606060);

		buffer.AddString(8 + pos, 2 + buffer.GetTextBaselinePos(16), 14, 0xFFFFFFFF, menus[i]->name);
		pos += width;
	}

	if (openedMenu != nullptr)
	{
		openedMenu->boundingBox.x = openedPos;
		openedMenu->boundingBox.y = 16;

		openedMenu->Draw(mouse, buffer);
	}
}

bool GuiMenuBar::CheckInputImpl(shared_ptr<Input> input)
{
	bool mouseDown = input->GetMouseButton(LEFT);
	ivec2 mouse = input->GetMousePos();
	bool b = false;
	
	if (GuiArea(0, 0, boundingBox.width, 16).HitTestAbs(mouse))
	{
		int pos = 0;
		for (int i = 0; i < menus.size(); i++)
		{
			int width = 16 + GuiRenderBuffer::GetTextWidth(menus[i]->name.length(), 14);
			if (GuiArea(pos, 0, width, 16).HitTestAbs(mouse))
			{
				if (openedMenu != nullptr)
					openedMenu->CloseSubmenus();

				if (mouseDown && !prevMouseState)
				{
					if (openedMenu == menus[i])
						openedMenu = nullptr;
					else
						openedMenu = menus[i];
				}
				else if (openedMenu != nullptr)
					openedMenu = menus[i];

				b = true;
				break;
			}
			pos += width;
		}

		if (mouseDown && !prevMouseState && !b)
		{
			if (openedMenu != nullptr)
				openedMenu->CloseSubmenus();
			openedMenu = nullptr;
		}
	}

	if (!b && openedMenu != nullptr && !openedMenu->CheckInput(input))
	{
		openedMenu->CloseSubmenus();
		openedMenu = nullptr;
	}

	prevMouseState = mouseDown;

	boundingBox = parent.lock()->boundingBox;
	boundingBox.x = boundingBox.y = 0;

	return false;
}

GuiMenuBar::GuiMenuBar() : GuiComponent(0, 0, 0, 20)
{
}

void GuiMenuBar::AddMenu(shared_ptr<GuiMenu> menu, int pos)
{
	if (pos == -1)
		pos = menus.size();
	else if ((unsigned) pos > menus.size())
		return;

	menus.insert(menus.begin() + pos, menu);
}

void GuiMenuBar::RemoveMenu(int pos)
{
	if (pos == -1)
		pos = menus.size() - 1;
	else if ((unsigned) pos > menus.size())
		return;

	menus.erase(menus.begin() + pos);
}

shared_ptr<GuiMenu> GuiMenuBar::GetMenu(int pos)
{
	if (pos == -1)
		pos = menus.size() - 1;
	else if ((unsigned) pos > menus.size())
		return nullptr;

	return menus[pos];
}