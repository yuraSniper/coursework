#include "GuiComboBox.h"

#include <Gui\Render\GuiRenderBuffer.h>
#include <Window\Input.h>

GuiComboBox::GuiComboBox(int x, int y, int width, int height) : GuiComponent(x, y, width, height)
{
}

void GuiComboBox::Draw(ivec2 mouse, GuiRenderBuffer & buffer)
{
	bool hovered = boundingBox.HitTestAbs(mouse);

	buffer.AddRectSized(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height, hovered? 0xFF007FFF : 0xFF505050);
	buffer.AddRectSized(boundingBox.x + 1, boundingBox.y + 1, boundingBox.width - 2 - 13, boundingBox.height - 2, 0xFF252525);
	buffer.AddRectSized(boundingBox.x + boundingBox.width - 13, boundingBox.y + 1, 12, boundingBox.height - 2, hovered ? 0xFF007FFF : 0xFF151515);
	buffer.AddTriangle(boundingBox.x + boundingBox.width - 14 + 3, boundingBox.y + boundingBox.height / 2 - 2, boundingBox.x + boundingBox.width - 14 + 6, boundingBox.y + boundingBox.height / 2 + 2, boundingBox.x + boundingBox.width - 14 + 10, boundingBox.y + boundingBox.height / 2 - 2, 0xFFFFFFFF);

	if (selectedIndex >= 0 && selectedIndex < items.size())
	{
		GuiArea tmp = GuiArea(boundingBox.x + 1, boundingBox.y + 1, boundingBox.width - 2 - 13, boundingBox.height - 2);
		tmp.MakeParent();

		buffer.AddString(1, buffer.GetTextBaselinePos(20), 14, 0xFFFFFFFF, items[selectedIndex]);

		tmp.UnmakeParent();
	}

	if (showItems)
	{
		list = GuiArea(boundingBox.x, boundingBox.y + boundingBox.height, boundingBox.width, 20 * glm::min((int)items.size(), 12));
		list.PushParent();
		int prevZ = buffer.SetZoffset(1000);
		buffer.AddRectSized(0, 0, list.width, list.height, 0xFF252525);

		int pos = (mouse.y - GuiArea::GetAbsOffset().y) / 20;

		if (list.HitTestAbs(mouse))
			buffer.AddRectSized(0, pos * 20, list.width, 20, 0xFF007FFF);

		for (int i = 0; i < items.size(); i++)
		{
			buffer.AddString(8, i * 20 + buffer.GetTextBaselinePos(20), 14, 0xFFFFFFFF, items[i]);
		}

		buffer.SetZoffset(prevZ);
		list.PopParent();
	}
}

bool GuiComboBox::CheckInput(shared_ptr<Input> input)
{
	bool mouseDown = input->GetMouseButton(LEFT);
	ivec2 mouse = input->GetMousePos();
	bool eatInput = false;

	if (!prevMouseState && mouseDown)
	{
		if (boundingBox.HitTestAbs(mouse))
			showItems = !showItems;
		else if (showItems && list.HitTestAbs(mouse, false))
		{
			eatInput = true;
			showItems = false;

			list.PushParent();

			mouse -= GuiArea::GetAbsOffset();
			int pos = mouse.y / 20;

			selectedIndex = pos;

			list.PopParent();
		}
		else
			showItems = false;
	}

	prevMouseState = mouseDown;

	return eatInput;
}

int GuiComboBox::GetSelectedIndex()
{
	return selectedIndex;
}

void GuiComboBox::SelectItem(int item)
{
	if (item >= -1 && item < items.size())
		selectedIndex = item;
}