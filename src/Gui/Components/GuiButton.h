#pragma once

#include <common.h>
#include <Util\Delegate.h>
#include <Gui\GuiComponent.h>

class Input;
class GuiRenderBuffer;

class GuiButton : public GuiComponent
{
	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInputImpl(shared_ptr<Input> input);

	bool isDown = false;
	bool prevMouseState = false;
public:
	Delegate<GuiComponent *> onAction;
	u16string text;

	GuiButton(u16string text = u"", int x = 0, int y = 0, int width = 0, int height = 0);
};