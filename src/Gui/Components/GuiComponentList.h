#pragma once

#include <common.h>
#include <Gui\GuiContainer.h>

class GuiComponentList : public GuiContainer
{
	virtual void DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer);
	
	void UpdateContent();
public:
	GuiComponentList(int x = 0, int y = 0, int width = 0, int height = 0);
};