#pragma once

#include <common.h>
#include <Gui\GuiComponent.h>

class GuiComboBox : public GuiComponent
{
	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer)
	{
	}
	
	virtual bool CheckInputImpl(shared_ptr<Input> input)
	{
		return false;
	}

	int selectedIndex = -1;
	bool showItems = false;
	bool prevMouseState = false;

	GuiArea list;

public:
	vector<u16string> items;

	GuiComboBox(int x = 0, int y = 0, int width = 0, int height = 0);

	virtual void Draw(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInput(shared_ptr<Input> input);

	int GetSelectedIndex();
	void SelectItem(int item);
};