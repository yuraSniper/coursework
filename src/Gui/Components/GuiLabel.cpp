#include "GuiLabel.h"

#include <Gui\Render\GuiRenderBuffer.h>

GuiLabel::GuiLabel(u16string text, int x, int y, int width, int height) : GuiComponent(x, y, width, height)
{
	this->text = text;
}

void GuiLabel::DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer)
{
	if (autoSize)
		boundingBox.width = buffer.GetTextWidth(text.length(), 14);

	buffer.AddString(0, buffer.GetTextBaselinePos(boundingBox.height), 14, 0xFFFFFFFF, text);
}

bool GuiLabel::CheckInputImpl(shared_ptr<Input> input)
{
	return false;
}