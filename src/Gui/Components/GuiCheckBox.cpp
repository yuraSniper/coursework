#include "GuiCheckBox.h"

#include <Gui\Render\GuiRenderBuffer.h>
#include <Window\Input.h>

void GuiCheckBox::DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer)
{
	if (autoSize)
		boundingBox.width = 20 + buffer.GetTextWidth(label.length(), 14);

	bool hovered = GuiArea(2, 2, 16, 16).HitTestAbs(mouse);

	buffer.AddRectSized(2, 2, 16, 16, 0xFF505050);
	buffer.AddRectSized(3, 3, 14, 14, hovered? 0xFF404040 : 0xFF353535);
	
	if (checked)
		buffer.AddRectSized(6, 6, 8, 8, 0xFF7F7F7F);

	buffer.AddString(20, buffer.GetTextBaselinePos(20), 14, 0xFFFFFFFF, label);
}

bool GuiCheckBox::CheckInputImpl(shared_ptr<Input> input)
{
	bool mouseDown = input->GetMouseButton(LEFT);
	ivec2 mouse = input->GetMousePos();

	if (GuiArea(2, 2, 16, 16).HitTestAbs(mouse))
	{
		if (!mouseDown && isDown)
			checked = !checked;

		if (mouseDown && !prevMouseState)
			isDown = true;
	}

	if (!mouseDown)
		isDown = false;

	prevMouseState = mouseDown;

	return false;
}

GuiCheckBox::GuiCheckBox(u16string label, int x, int y, int width, int height) : GuiComponent(x, y, width, height)
{
	this->label = label;
}