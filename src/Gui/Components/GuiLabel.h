#pragma once

#include <common.h>
#include <Gui\GuiComponent.h>

class GuiLabel : public GuiComponent
{
	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInputImpl(shared_ptr<Input> input);
public:
	u16string text;
	bool autoSize = true;

	GuiLabel(u16string text = u"", int x = 0, int y = 0, int width = 0, int height = 0);
};