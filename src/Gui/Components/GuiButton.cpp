#include "GuiButton.h"

#include <Gui\Render\GuiRenderBuffer.h>
#include <Render\TextRenderer.h>
#include <Window\Input.h>

void GuiButton::DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer)
{
	bool hovered = clampedBoundingBox.HitTestAbs(mouse);

	buffer.AddRectSized(0, 0, boundingBox.width, boundingBox.height, 0xFF505050);
	buffer.AddRectSized(1, 1, boundingBox.width - 2, boundingBox.height - 2, isDown ? 0xFF252525 : hovered? 0xFF404040 : 0xFF353535);

	buffer.AddString(glm::max(0.0f, boundingBox.width - buffer.GetTextWidth(text, 14)) / 2.0f, 1 + buffer.GetTextBaselinePos(boundingBox.height - 2), 14, 0xFFFFFFFF, text);
}

bool GuiButton::CheckInputImpl(shared_ptr<Input> input)
{
	ivec2 mouse = input->GetMousePos();
	bool mouseDown = input->GetMouseButton(LEFT);

	if (clampedBoundingBox.HitTestAbs(mouse))
	{
		if (!mouseDown && isDown)
			onAction(this);

		if (mouseDown && !prevMouseState)
			isDown = true;
	}

	if (!mouseDown)
		isDown = false;

	prevMouseState = mouseDown;

	return false;
}

GuiButton::GuiButton(u16string text, int x, int y, int width, int height) : GuiComponent(x, y, width, height)
{
	this->text = text;
}