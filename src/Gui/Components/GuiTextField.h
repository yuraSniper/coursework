#pragma once

#include <common.h>
#include <Gui\GuiComponent.h>

class GuiTextField : public GuiComponent
{
	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInputImpl(shared_ptr<Input> input);

	void Copy(int start, int end);
	void Cut(int start, int end);
	void Paste(int start, int end);

	bool wasFocused = false;
	bool selecting = false;
	bool prevMouseDown = false;
	int offset = 0;
	int caretPos = 0;
	int selectionStart = 0;

	int caretBlinkCounter = 0;
public:
	u16string text;
	u16string hintText = u"";
	bool numberOnly = false;
	int maxLength = -1;

	GuiTextField(u16string text = u"", int x = 0, int y = 0, int width = 0, int height = 0);

	virtual bool CanHaveFocus();
};