#pragma once

#include <common.h>
#include <Gui\GuiContainer.h>

class GuiStringList : public GuiContainer
{
	virtual void DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer);

	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInputImpl(shared_ptr<Input> input);

	vector<u16string> list;
	int selected = -1;
public:
	GuiStringList(int x = 0, int y = 0, int width = 0, int height = 0);
	
	void AddItem(u16string item, int pos = -1);
	u16string GetItem(int index = -1);
	void RemoveItem(int index = -1);

	int GetSelectedItemIndex();
	void SelectItem(int index = -1);

	virtual bool CanHaveFocus();
};