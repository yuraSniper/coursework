#include "GuiStringList.h"

#include <Gui\Render\GuiRenderBuffer.h>
#include <Window\Input.h>

void GuiStringList::DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer)
{
	interiorBoundingBox.width = 0;
	interiorBoundingBox.height = list.size() * 16;

	for (int i = 0; i < list.size(); i++)
		interiorBoundingBox.width = glm::max(interiorBoundingBox.width, (int)floor(buffer.GetTextWidth(list[i].length(), 14)) + 1);
}

void GuiStringList::DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer)
{
	for (int i = 0; i < list.size(); i++)
	{
		if (i == selected)
			buffer.AddRectSized(0, i * 16, boundingBox.width, 16, 0xFF007FFF);
		else if (GuiArea(0, i * 16, boundingBox.width, 16).IntersectWithParent().HitTestAbs(mouse))
			buffer.AddRectSized(0, i * 16, boundingBox.width, 16, 0xFF353535);
		buffer.AddString(0, i * 16 + buffer.GetTextBaselinePos(14) + 1, 14, 0xFFFFFFFF, list[i]);
	}
}

bool GuiStringList::CheckInputImpl(shared_ptr<Input> input)
{
	bool mouseDown = input->GetMouseButton(LEFT);
	ivec2 mouse = input->GetMousePos();

	if (visibleContent.HitTestAbs(mouse) && mouseDown && !prevMouseDown)
	{
		mouse -= GuiArea::GetAbsOffset();
		selected = mouse.y / 16;
	}

	prevMouseDown = mouseDown;

	return false;
}

GuiStringList::GuiStringList(int x, int y, int width, int height) : GuiContainer(x, y, width, height)
{
	color = 0xFF202020;
}

void GuiStringList::AddItem(u16string item, int pos)
{
	if (pos == -1)
		pos = list.size();
	else if ((unsigned)pos > list.size())
		return;
	
	list.insert(list.begin() + pos, item);
}

u16string GuiStringList::GetItem(int index)
{
	if (index == -1)
		index = list.size() - 1;
	else if ((unsigned) index >= list.size())
		return u"";

	return list[index];
}

void GuiStringList::RemoveItem(int index)
{
	if (index == -1)
		list.pop_back();
	else if ((unsigned) index >= list.size())
		return;

	list.erase(list.begin() + index);
}

int GuiStringList::GetSelectedItemIndex()
{
	return selected;
}

void GuiStringList::SelectItem(int index)
{
	if (index != -1 && (unsigned) index >= list.size())
		return;

	selected = index;
}

bool GuiStringList::CanHaveFocus()
{
	return true;
}