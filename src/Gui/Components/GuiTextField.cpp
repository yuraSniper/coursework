#include "GuiTextField.h"

#include <Main\Main.h>
#include <Gui\Render\GuiRenderBuffer.h>
#include <Gui\GuiContainer.h>
#include <Window\Input.h>
#include <Window\Window.h>

void GuiTextField::DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer)
{
	bool hovered = clampedBoundingBox.HitTestAbs(mouse);
	float glyphWidth = buffer.GetTextWidth(1, 14);

	buffer.AddRectSized(0, 0, boundingBox.width, boundingBox.height, hovered ? 0xFF007FFF : IsFocused()? 0xFF003F7F : 0xFF505050);
	buffer.AddRectSized(1, 1, boundingBox.width - 2, boundingBox.height - 2, 0xFF252525);
	
	if (selectionStart != caretPos)
		buffer.AddRect(2 + (selectionStart - offset) * glyphWidth, 2, 2 + (caretPos - offset) * glyphWidth, boundingBox.height - 4, IsFocused()? 0xFF007FFF : 0xFF606060);
	
	buffer.AddString(2 - offset * glyphWidth, 1 + buffer.GetTextBaselinePos(boundingBox.height - 2), 14, 0xFFFFFFFF, text);
	if (text == u"")
		buffer.AddString(2 - offset * glyphWidth, 1 + buffer.GetTextBaselinePos(boundingBox.height - 2), 14, 0xFF7F7F7F, hintText);

	if (IsFocused() && caretBlinkCounter >= 60)
		buffer.AddLine(3 + (caretPos - offset) * glyphWidth, 2, 2 + (caretPos - offset) * glyphWidth, boundingBox.height - 4, 0xFFFFFFFF);
}

bool GuiTextField::CheckInputImpl(shared_ptr<Input> input)
{
	bool mouseDown = input->GetMouseButton(LEFT);
	ivec2 mousePos = input->GetMousePos();

	caretBlinkCounter++;
	if (caretBlinkCounter >= 120)
		caretBlinkCounter = 0;

	if (!mouseDown)
		selecting = false;

	if (IsFocused())
	{
		char16_t ch = input->GetLastChar();
		unsigned short key = input->GetLastKeyDown();

		bool shiftDown = input->GetKey(VK_LSHIFT) | input->GetKey(VK_RSHIFT);
		bool ctrlDown = input->GetKey(VK_LCONTROL) | input->GetKey(VK_RCONTROL);

		if (mouseDown && clampedBoundingBox.HitTestAbs(mousePos))
		{
			if (!prevMouseDown)
				selectionStart = clamp((int) floor((mousePos - GuiArea::GetAbsOffset() - 2).x / GuiRenderBuffer::GetTextWidth(1, 14) + 0.5f), 0, (int) text.length());
			selecting = true;
		}

		if (selecting)
			caretPos = clamp((int) floor((mousePos - GuiArea::GetAbsOffset() - 2).x / GuiRenderBuffer::GetTextWidth(1, 14) + 0.5f), 0, (int) text.length());

		if (wasFocused)
		{
			int selStart = glm::min(caretPos, selectionStart);
			int selEnd = glm::max(caretPos, selectionStart);

			if (ctrlDown && !shiftDown)
			{
				if (key == 'A')
				{
					selectionStart = 0;
					caretPos = text.length();
				}
				else if (key == 'C' || key == VK_INSERT)
					Copy(selStart, selEnd);
				else if (key == 'V')
					Paste(selStart, selEnd);
				else if (key == 'X')
					Cut(selStart, selEnd);
				
				ch = 0;
			}
			else if (shiftDown)
			{
				if (key == VK_INSERT)
				{
					Paste(selStart, selEnd);
					ch = 0;
				}
				else if (key == VK_DELETE)
				{
					Cut(selStart, selEnd);
					ch = 0;
				}
			}

			switch (key)
			{
				case '\b':
					if (caretPos != selectionStart)
					{
						text.erase(selStart, selEnd - selStart);
						selectionStart = caretPos = selStart;
					}
					else if (caretPos > 0)
					{
						text.erase(--caretPos, 1);
						selectionStart = caretPos;
					}
					caretBlinkCounter = 60;
					break;
				case VK_DELETE:
					if (caretPos != selectionStart)
					{
						text.erase(selStart, selEnd - selStart);
						selectionStart = caretPos = selStart;
					}
					else if (caretPos < text.length())
						text.erase(caretPos, 1);
					caretBlinkCounter = 60;
					break;
				case VK_LEFT:
					if (caretPos > 0)
						caretPos--;
					if (!shiftDown)
						selectionStart = caretPos;
					caretBlinkCounter = 60;
					break;
				case VK_RIGHT:
					if (caretPos < text.length())
						caretPos++;
					if (!shiftDown)
						selectionStart = caretPos;
					caretBlinkCounter = 60;
					break;
				case VK_HOME:
					caretPos = 0;
					if (!shiftDown)
						selectionStart = caretPos;
					caretBlinkCounter = 60;
					break;
				case VK_END:
					caretPos = text.length();
					if (!shiftDown)
						selectionStart = caretPos;
					caretBlinkCounter = 60;
					break;
				default:
					if (ch >= 32)
					{
						if (!numberOnly || (ch >= '0' && ch <= '9'))
						if (maxLength == -1 || text.length() - (selEnd - selStart) + 1 <= maxLength)
						{
							text.replace(text.begin() + selStart, text.begin() + selEnd, 1, ch);

							caretPos = glm::min((int) text.length(), selStart + 1);
							selectionStart = caretPos;
						}
					}
			}
		}
	}

	wasFocused = IsFocused();
	prevMouseDown = mouseDown;

	return false;
}

void GuiTextField::Copy(int start, int end)
{
	Main::GetInstance().mainWindow->PutStrInClipboard(text.substr(start, end - start));
}

void GuiTextField::Cut(int start, int end)
{
	Main::GetInstance().mainWindow->PutStrInClipboard(text.substr(start, end - start));

	text.erase(start, end - start);
	selectionStart = caretPos = start;
}

void GuiTextField::Paste(int start, int end)
{
	u16string str = Main::GetInstance().mainWindow->GetStrFromClipboard();

	if (str != u"")
	{
		text.reserve(text.length() + str.length());
		str = str.substr(0, str.find('\r'));
		text.replace(text.begin() + start, text.begin() + end, str.c_str());
		selectionStart = caretPos = start + str.length();
	}
}

GuiTextField::GuiTextField(u16string text, int x, int y, int width, int height) : GuiComponent(x, y, width, height)
{
	this->text = text;
}

bool GuiTextField::CanHaveFocus()
{
	return true;
}