#pragma once

#include <common.h>
#include <Gui\GuiComponent.h>

class GuiCheckBox : public GuiComponent
{
	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInputImpl(shared_ptr<Input> input);

	bool prevMouseState = false;
	bool isDown = false;
public:
	u16string label;
	bool checked = false;
	bool autoSize = true;

	GuiCheckBox(u16string label = u"", int x = 0, int y = 0, int width = 0, int height = 0);
};