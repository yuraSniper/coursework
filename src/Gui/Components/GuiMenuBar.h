#pragma once

#include <common.h>
#include <Gui\GuiComponent.h>

class GuiMenu;

class GuiMenuBar : public GuiComponent
{
	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInputImpl(shared_ptr<Input> input);

	vector<shared_ptr<GuiMenu>> menus;

	shared_ptr<GuiMenu> openedMenu = nullptr;
	bool prevMouseState = false;
public:
	GuiMenuBar();

	void AddMenu(shared_ptr<GuiMenu> menu, int pos = -1);
	void RemoveMenu(int pos = -1);
	shared_ptr<GuiMenu> GetMenu(int pos = -1);
};