#pragma once

#include <common.h>
#include <Thread\Thread.h>
#include <Main\Main.h>

class RenderThread : public Thread
{
public:
	void run()
	{
		Main & renderer = Main::GetInstance();
		renderer.InitGL();

		while (true)
		{
			renderer.DrawFrame();
		}
	}
};