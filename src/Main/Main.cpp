#include "Main.h"

#include <Gui\GuiArea.h>
#include <Gui\Render\GuiRenderBuffer.h>
#include <Main\Thread\RenderThread.h>
#include <Render\Buffer\RenderBuffer.h>
#include <Render\Shader\ShaderProgram.h>
#include <Render\Resource\ResourceManager.h>
#include <Window\Input.h>
#include <Window\Window.h>
#include <Main\Gui\GuiTest.h>

Main & Main::GetInstance()
{
	static Main instance;
	return instance;
}

void Main::Start()
{
	mainWindow = make_shared<Window>(800, 600);
	mainWindow->onCLose.Add(this, &Main::OnClose);

	renderThread = make_shared<RenderThread>();

	input = make_shared<Input>(mainWindow);
	mainWindow->SetVisibility(true);

	Init();

	MainLoop();

	renderThread->terminate();
}

void Main::OnClose()
{
	working = false;
}

void Main::MainLoop()
{
	renderThread->resume();
	while (working)
	{
		if (!mainWindow->MessageLoop())
			working = false;
		Sleep(1);
	}
}

void Main::DrawArea(GuiArea area, vec4 color, RenderBuffer & buffer)
{
	area = area.MakeAbsolute();
	color *= 255.0f;
	int c = ((int) color.r << 24) | ((int) color.g << 16) | ((int) color.b << 8) | (int) color.a;
	
	buffer.AddVertex(Vertex(area.x, area.y, 0, 0, 0, 0, 0, c));
	buffer.AddVertex(Vertex(area.x, area.y + area.height - 1, 0, 0, 1, 0, 0, c));
	buffer.AddVertex(Vertex(area.x + area.width - 1, area.y, 0, 1, 0, 0, 0, c));
	buffer.AddVertex(Vertex(area.x + area.width - 1, area.y + area.height - 1, 0, 1, 1, 0, 0, c));
}

void Main::Init()
{
	currentGui = make_shared<GuiTest>();
	currentGui->Init();
}

void Main::InitGL()
{
	rc = wglCreateContext(mainWindow->GetHDC());
	wglMakeCurrent(mainWindow->GetHDC(), rc);

	cout << "GL_VERSION : " << glGetString(GL_VERSION) << endl;

	glewInit();
	if (__WGLEW_EXT_swap_control)
		wglSwapIntervalEXT(0);
	else
		cout << "wglSwapIntervalEXT is not supported." << endl;

	glClearColor(0.1, 0.1, 0.1, 1);

	glEnable(GL_DEPTH_TEST);
	glClearDepth(1);
	glDepthFunc(GL_LEQUAL);

	guiShader = ShaderProgram::CreateFromFile("data\\Shaders\\Gui.glsl");
	guiShader->Bind();

	glUniform1i(guiShader->GetUniformLocation("tex"), 1);

	ResourceManager::GetInstance().LoadTextureNotFound(2);
	font = ResourceManager::GetInstance().LoadFont("data\\Font");

	font.Bind(1);

	//TextRenderer::GetInstance().SetSize(20);
}

void Main::DrawFrame()
{
	static GuiRenderBuffer guiBuffer(guiShader, &font);
	int width = mainWindow->GetClientWidth();
	int height = mainWindow->GetClientHeight();

	if (width == 0 || height == 0)
		return;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glViewport(0, 0, width, height);

	projection = ortho<float>(0, width, height, 0, -1000, 1000);
	view = mat4();
	model = mat4();
	guiShader->Bind();
	glUniformMatrix4fv(guiShader->GetUniformLocation("mvp"), 1, false, value_ptr(GetMVP()));

	GuiArea base = GuiArea(0, 0, width, height);

	if (input->GetKeyPressed(VK_F4))
	{
		guiShader->Reload();
	}

	base.MakeParent();

	ivec2 mouse = input->GetMousePos();
	
	currentGui->boundingBox = base;
	currentGui->Draw(mouse, guiBuffer);
	currentGui->CheckInput(input);

	guiBuffer.Draw();
	guiBuffer.Clear();

	base.UnmakeParent();

	mainWindow->SwapBuffers();
	Sleep(1);
}

mat4 Main::GetMVP()
{
	return projection * view * model;
}

glm::mat4 Main::GetVP()
{
	return projection * view;
}