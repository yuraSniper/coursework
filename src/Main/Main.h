#pragma once

#include <common.h>
#include <glm.h>
#include <Render\Resource\Font.h>

class Window;
class Input;
class Thread;
class GuiArea;
class Gui;
class ShaderProgram;
class RenderBuffer;

class Main
{
	Main()
	{
	}
	Main(Main &);
	void operator=(Main &);

	shared_ptr<Thread> renderThread;

	HGLRC rc;

	bool working = true;

	void OnClose();
	void MainLoop();
	void DrawArea(GuiArea area, vec4 color, RenderBuffer & buffer);
	void Init();

	mat4 projection;
	mat4 view;
	mat4 model;

	shared_ptr<Gui> currentGui;

	shared_ptr<ShaderProgram> guiShader;
	Font font;

public:
	shared_ptr<Window> mainWindow;
	shared_ptr<Input> input;
	
	static Main & GetInstance();

	void Start();
	void InitGL();
	void DrawFrame();
	mat4 GetMVP();
	mat4 GetVP();
};