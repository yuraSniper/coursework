﻿#include "GuiTest.h"

#include <Gui\Components\GuiButton.h>
#include <Gui\Components\GuiLabel.h>
#include <Gui\Components\GuiTextField.h>
#include <Gui\Components\GuiScrollBar.h>
#include <Gui\Components\GuiCheckBox.h>
#include <Gui\Components\GuiComboBox.h>
#include <Gui\Components\GuiComponentList.h>
#include <Gui\Components\GuiStringList.h>
#include <Gui\Components\GuiMenuBar.h>
#include <Gui\Components\SubComponents\GuiMenu.h>
#include <Gui\GuiContainer.h>
#include <Gui\Render\GuiRenderBuffer.h>
#include <Main\Main.h>
#include <Main\Gui\Components\GuiFamilyTree.h>
#include <Main\Gui\Components\SubComponents\GuiFamilyTreeNode.h>
#include <Util\ByteStream.h>
#include <Window\Input.h>
#include <Window\SelectFileDialog.h>

GuiTest::GuiTest(int x, int y, int width, int height) : Gui(x, y, width, height)
{
	color = 0xFF101010;
}

void GuiTest::Init()
{
	u16string fileMenuItems[] = {u"Створити", u"Відкрити...", u"Зберегти", u"Зберегти як...", u"Вихід"};
	u16string editMenuItems[] = {u"Додати вузол", u"Видалити вузол"};
	u16string helpMenuItems[] = {u"Про програму"};
	shared_ptr<GuiMenu> fileMenu = make_shared<GuiMenu>(u"Файл");
	shared_ptr<GuiMenu> editMenu = make_shared<GuiMenu>(u"Дерево");
	shared_ptr<GuiMenu> helpMenu = make_shared<GuiMenu>(u"Довідка");
	
	fileMenu->onAction.Add(this, &GuiTest::FileMenuHandler);
	editMenu->onAction.Add(this, &GuiTest::EditMenuHandler);
	helpMenu->onAction.Add(this, &GuiTest::HelpMenuHandler);

	for (int i = 0; i < 5; i++)
		fileMenu->AddItem(fileMenuItems[i]);

	for (int i = 0; i < 2; i++)
		editMenu->AddItem(editMenuItems[i]);

	for (int i = 0; i < 1; i++)
		helpMenu->AddItem(helpMenuItems[i]);

	AddComponent(theMenuBar = make_shared<GuiMenuBar>());
	theMenuBar->AddMenu(fileMenu);
	theMenuBar->AddMenu(editMenu);
	theMenuBar->AddMenu(helpMenu);

	theTree = make_shared<GuiFamilyTree>();
	
	AddComponent(theTree);
}

void GuiTest::DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer)
{
	interiorBoundingBox = GuiArea(0, 0, boundingBox.width, boundingBox.height);
	theTree->boundingBox = GuiArea(5, 16 + 5, boundingBox.width - 10, boundingBox.height - 30);
}

bool GuiTest::CheckInputImplPost(shared_ptr<Input> input, bool inputEaten)
{
	if (input->GetKeyPressed('S') && input->GetKey(VK_CONTROL))
	{
		string fileName = SelectFileDialog::SaveFile("Tree(*.tree)");

		if (fileName != "")
		{
			ByteStream stream;

			theTree->Serialize(stream);

			ofstream file;
			file.open(fileName, ios::binary | ios::out);

			vector<unsigned char> & data = stream.GetData();
			file.write((char *)data.data(), data.size());

			file.close();
		}
	}

	if (input->GetKeyPressed('O') && input->GetKey(VK_CONTROL))
	{
		string fileName = SelectFileDialog::OpenFile("Tree(*.tree)");

		if (fileName != "")
		{
			ByteStream stream;

			ifstream file;
			file.open(fileName, ios::binary | ios::in);

			vector<unsigned char> & data = stream.GetData();
			data.assign(istreambuf_iterator<char>(file), istreambuf_iterator<char>());

			theTree->Deserialize(stream);

			file.close();
		}
	}

	return false;
}

void GuiTest::DrawImplPost(ivec2 mouse, GuiRenderBuffer & buffer)
{
	//buffer.AddSpline(p1.x, p1.y, p2.x, p2.y, 0xFF007FFF);
}