#pragma once

#include <common.h>
#include <Gui\Gui.h>

class GuiMenuBar;
class GuiMenuItem;
class GuiFamilyTree;

class GuiTest : public Gui
{
	shared_ptr<GuiMenuBar> theMenuBar;
	shared_ptr<GuiFamilyTree> theTree;


public:
	GuiTest(int x = 0, int y = 0, int width = 0, int height = 0);

	virtual void Init();
	virtual void DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInputImplPost(shared_ptr<Input> input, bool inputEaten);
	virtual void DrawImplPost(ivec2 mouse, GuiRenderBuffer & buffer);
};