#pragma once

#include <common.h>
#include <Gui\Guicontainer.h>

class GuiFamilyTreeNode;
class ByteStream;

struct NodeConnection
{
	int index;
	int child;
	int parent1, parent2;
};

class GuiFamilyTree : public GuiContainer
{
	virtual void DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual void DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual bool CheckInputImplPost(shared_ptr<Input> input, bool inputEaten);

	void OnDel(GuiComponent *);
	void DisconnectNode(GuiFamilyTreeNode * node);
	int IndexOf(GuiFamilyTreeNode * node);

	shared_ptr<GuiFamilyTreeNode> connectingFrom;
	int connectingFromSocketId = -1;

	bool prevMouseState = false;
public:
	GuiFamilyTree(int x = 0, int y = 0, int width = 0, int height = 0);

	void Serialize(ByteStream & stream);
	void Deserialize(ByteStream & stream);
};