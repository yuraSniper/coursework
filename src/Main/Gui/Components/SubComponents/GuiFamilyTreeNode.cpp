﻿#include "GuiFamilyTreeNode.h"

#include <Gui\Render\GuiRenderBuffer.h>
#include <Gui\Components\GuiTextField.h>
#include <Gui\Components\GuiComboBox.h>
#include <Gui\Components\GuiButton.h>
#include <Util\ByteStream.h>
#include <Window\Input.h>

bool GuiFamilyTreeNode::CheckInputImplPost(shared_ptr<Input> input, bool inputEaten)
{
	bool mouseDown = input->GetMouseButton(LEFT);
	ivec2 mouse = input->GetMousePos();
	bool eatInput = false;

	if (IsFocused() && mouseDown && !prevMouseState && GuiArea(1, 1, boundingBox.width - 2, 8).HitTestAbs(mouse))
	{
		moving = true;
		eatInput = true;
		prevMousePos = mouse;
	}
	
	if (moving)
	{
		ivec2 delta = prevMousePos - mouse;
		boundingBox.x -= delta.x;
		boundingBox.y -= delta.y;
		
		prevMousePos = mouse;
	}

	if (!mouseDown)
		moving = false;
	
	prevMouseState = mouseDown;

	return eatInput;
}

void GuiFamilyTreeNode::DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer)
{
	if (clampedBoundingBox.HitTestAbs(mouse))
		color = 0xFF353535;
	else
		color = 0xFF303030;
}

void GuiFamilyTreeNode::DrawImplPost(ivec2 mouse, GuiRenderBuffer & buffer)
{
	buffer.AddRectSized(1, 1, boundingBox.width - 2, 8, 0xFF404040);

	if (IsFocused())
	{
		buffer.AddRectOutline(0, 0, boundingBox.width, boundingBox.height, 0xFF004F7F);
	}
}

GuiFamilyTreeNode::GuiFamilyTreeNode(int x, int y) : GuiContainer(x, y, 205, 115)
{
	showHorizontalScrollBar = showVerticalScrollBar = false;
	releaseFocus = true;
}

void GuiFamilyTreeNode::Serialize(ByteStream & stream)
{
	if (sex != nullptr)
	{
		stream.Write(boundingBox.x);
		stream.Write(boundingBox.y);
		stream.Write(firstName->text);
		stream.Write(lastName->text);
		stream.Write(day->text);
		stream.Write(month->GetSelectedIndex());
		stream.Write(year->text);
		stream.Write(sex->GetSelectedIndex());
	}
}

void GuiFamilyTreeNode::Deserialize(ByteStream & stream)
{
	if (sex != nullptr)
	{
		int tmp;
		stream.Read(boundingBox.x);
		stream.Read(boundingBox.y);
		stream.Read(firstName->text);
		stream.Read(lastName->text);
		stream.Read(day->text);
		stream.Read(tmp);
		month->SelectItem(tmp);
		stream.Read(year->text);
		stream.Read(tmp);
		sex->SelectItem(tmp);
	}
}

void GuiFamilyTreeNode::Init()
{
	static vector<u16string> months = {u"Січень", u"Лютий", u"Березень", u"Квітень", u"Травень", u"Червень", u"Липень", u"Серпень", u"Вересень", u"Жовтень", u"Листопад", u"Грудень"};
	if (sex == nullptr)
	{
		AddComponent(firstName = make_shared<GuiTextField>(u"", 5, 15, 195, 20));
		AddComponent(lastName = make_shared<GuiTextField>(u"", 5, 40, 195, 20));

		AddComponent(day = make_shared<GuiTextField>(u"", 5, 65, 40, 20));
		AddComponent(month = make_shared<GuiComboBox>(50, 65, 80, 20));
		AddComponent(year = make_shared<GuiTextField>(u"", 135, 65, 65, 20));

		AddComponent(sex = make_shared<GuiComboBox>(5, 90, 70, 20));
		AddComponent(del = make_shared<GuiButton>(u"Delete", 140, 90, 60, 20));

		firstName->hintText = u"Ім'я";
		lastName->hintText = u"Прізвище";
		year->hintText = u"Рік";
		day->hintText = u"День";

		year->numberOnly = true;
		year->maxLength = 4;
		day->numberOnly = true;
		day->maxLength = 2;

		sex->items.push_back(u"чол");
		sex->items.push_back(u"жін");

		month->items = months;
	}
}

bool GuiFamilyTreeNode::CanHaveFocus()
{
	return true;
}