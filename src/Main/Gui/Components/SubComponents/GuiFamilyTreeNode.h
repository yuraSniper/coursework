#pragma once

#include <common.h>
#include <Gui\GuiContainer.h>

class GuiTextField;
class GuiButton;
class GuiComboBox;
class ByteStream;

class GuiFamilyTreeNode : public GuiContainer
{
	virtual bool CheckInputImplPost(shared_ptr<Input> input, bool inputEaten);
	virtual void DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer);
	virtual void DrawImplPost(ivec2 mouse, GuiRenderBuffer & buffer);

	bool moving = false;
	bool prevMouseState = false;
	ivec2 prevMousePos;

public:
	GuiFamilyTreeNode * child = nullptr;
	GuiFamilyTreeNode * parent1 = nullptr;
	GuiFamilyTreeNode * parent2 = nullptr;

	shared_ptr<GuiTextField> firstName, lastName;
	shared_ptr<GuiTextField> day;
	shared_ptr<GuiComboBox> month;
	shared_ptr<GuiTextField> year;
	shared_ptr<GuiComboBox> sex;
	shared_ptr<GuiButton> del;

	GuiFamilyTreeNode(int x = 0, int y = 0);

	void Serialize(ByteStream & stream);
	void Deserialize(ByteStream & stream);

	void Init();
	virtual bool CanHaveFocus();
};