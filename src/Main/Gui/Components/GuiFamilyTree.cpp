#include "GuiFamilyTree.h"

#include <Gui\Render\GuiRenderBuffer.h>
#include <Main\Gui\Components\SubComponents\GuiFamilyTreeNode.h>
#include <Gui\Components\GuiButton.h>
#include <Window\Input.h>
#include <Util\ByteStream.h>
#include <Util\Delegate.h>

void GuiFamilyTree::DrawImplPre(ivec2 mouse, GuiRenderBuffer & buffer)
{
	buffer.AddRectSized(0, 0, boundingBox.width, boundingBox.height, 0xFF181818);

	ivec2 delta = {interiorBoundingBox.x, interiorBoundingBox.y};
	ivec2 p1 = {0, 0}, p2 = {boundingBox.width, boundingBox.height};

	for (int i = 0; i < components.size(); i++)
	{
		if (components[i] == nullptr)
			continue;

		p2.x = glm::max(p2.x, components[i]->boundingBox.x + components[i]->boundingBox.width);
		p2.y = glm::max(p2.y, components[i]->boundingBox.y + components[i]->boundingBox.height);
	}
	
	p2 -= p1 - 100;
	//interiorBoundingBox = GuiArea(p1.x, p1.y, p2.x, p2.y);

	interiorBoundingBox.width = p2.x;
	interiorBoundingBox.height = p2.y;
}

GuiFamilyTree::GuiFamilyTree(int x, int y, int width, int height) : GuiContainer(x, y, width, height)
{
	releaseFocus = true;
}

void GuiFamilyTree::Serialize(ByteStream & stream)
{
	vector<NodeConnection> connections;
	stream.Write((int)components.size());
	for (int i = 0; i < components.size(); i++)
	{
		shared_ptr<GuiFamilyTreeNode> node = dynamic_pointer_cast<GuiFamilyTreeNode>(components[i]);

		node->Serialize(stream);

		NodeConnection conn;

		conn.index = i;
		conn.child = IndexOf(node->child);
		conn.parent1 = IndexOf(node->parent1);
		conn.parent2 = IndexOf(node->parent2);

		connections.push_back(conn);
	}

	stream.Write((int)connections.size());
	stream.Write(connections.data(), connections.size() * sizeof(NodeConnection));
}

void GuiFamilyTree::Deserialize(ByteStream & stream)
{
	int componentCount;
	stream.Read(componentCount);

	components.clear();

	for (int i = 0; i < componentCount; i++)
	{
		shared_ptr<GuiFamilyTreeNode> node = make_shared<GuiFamilyTreeNode>();
		AddComponent(node);
		
		node->Init();
		node->Deserialize(stream);
		node->del->onAction.Add(this, &GuiFamilyTree::OnDel);
	}
	int tmp;
	stream.Read(tmp);

	NodeConnection * connections = new NodeConnection[tmp];
	stream.Read(connections, tmp * sizeof(NodeConnection));

	for (int i = 0; i < tmp; i++)
	{
		shared_ptr<GuiFamilyTreeNode> node = dynamic_pointer_cast<GuiFamilyTreeNode>(components[i]);

		node->child = (GuiFamilyTreeNode *)GetComponent(connections[i].child).get();
		node->parent1 = (GuiFamilyTreeNode *)GetComponent(connections[i].parent1).get();
		node->parent2 = (GuiFamilyTreeNode *)GetComponent(connections[i].parent2).get();
	}
}

void GuiFamilyTree::DrawImpl(ivec2 mouse, GuiRenderBuffer & buffer)
{
	for (int i = 0; i < components.size(); i++)
	{
		shared_ptr<GuiFamilyTreeNode> node = dynamic_pointer_cast<GuiFamilyTreeNode>(components[i]);

		if (node == nullptr)
			continue;

		if (node->child != nullptr)
		{
			GuiFamilyTreeNode * child = node->child;

			ivec2 offset = ivec2(node->boundingBox.x, node->boundingBox.y);
			ivec2 childOffset = ivec2(child->boundingBox.x, child->boundingBox.y);
			ivec2 size = ivec2(node->boundingBox.width, node->boundingBox.height);

			GuiArea socket0 = GuiArea(offset.x + size.x / 2 - 5, offset.y - 5, 10, 10);
			GuiArea socket1 = GuiArea(childOffset.x + size.x / 4, childOffset.y + size.y - 5, 10, 10);
			GuiArea socket2 = GuiArea(childOffset.x + size.x / 4 + size.x / 2, childOffset.y + size.y - 5, 10, 10);

			if (child->parent1 == node.get())
				buffer.AddSpline(socket0.x + socket0.width / 2, socket0.y + socket0.height / 2, socket1.x + socket1.width / 2, socket1.y + socket1.height / 2, 0xFF007FFF);
			else if (child->parent2 == node.get())
				buffer.AddSpline(socket0.x + socket0.width / 2, socket0.y + socket0.height / 2, socket2.x + socket2.width / 2, socket2.y + socket2.height / 2, 0xFF007FFF);
		}
	}
	
	buffer.IncrementZoffset();

	for (int i = 0; i < components.size(); i++)
	{
		if (components[i] == nullptr)
			continue;

		shared_ptr<GuiFamilyTreeNode> node = dynamic_pointer_cast<GuiFamilyTreeNode>(components[i]);

		ivec2 offset = ivec2(node->boundingBox.x, node->boundingBox.y);
		ivec2 size = ivec2(node->boundingBox.width, node->boundingBox.height);

		GuiArea socket0 = GuiArea(offset.x + size.x / 2 - 5, offset.y - 5, 10, 10);
		GuiArea socket1 = GuiArea(offset.x + size.x / 4, offset.y + size.y - 5, 10, 10);
		GuiArea socket2 = GuiArea(offset.x + size.x / 4 + size.x / 2, offset.y + size.y - 5, 10, 10);

		buffer.AddRectSized(socket1, socket1.HitTestAbs(mouse) ? 0xFF007FFF : 0xFF003F7F);
		buffer.AddRectSized(socket2, socket2.HitTestAbs(mouse) ? 0xFF007FFF : 0xFF003F7F);
		buffer.AddRectSized(socket0, socket0.HitTestAbs(mouse) ? 0xFF007FFF : 0xFF003F7F);
		
		node->Draw(mouse, buffer);
	}

	if (connectingFromSocketId != -1)
	{
		ivec2 offset = ivec2(connectingFrom->boundingBox.x, connectingFrom->boundingBox.y);
		ivec2 size = ivec2(connectingFrom->boundingBox.width, connectingFrom->boundingBox.height);

		GuiArea socket0 = GuiArea(offset.x + size.x / 2 - 5, offset.y - 5, 10, 10);
		GuiArea socket1 = GuiArea(offset.x + size.x / 4, offset.y + size.y - 5, 10, 10);
		GuiArea socket2 = GuiArea(offset.x + size.x / 4 + size.x / 2, offset.y + size.y - 5, 10, 10);

		mouse -= GuiArea::GetAbsOffset();

		switch (connectingFromSocketId)
		{
			case 0:
				buffer.AddSpline(socket0.x + socket0.width / 2, socket0.y + socket0.height / 2, mouse.x, mouse.y, 0xFF007FFF);
				break;
			case 1:
				buffer.AddSpline(socket1.x + socket1.width / 2, socket1.y + socket2.height / 2, mouse.x, mouse.y, 0xFF007FFF);
				break;
			case 2:
				buffer.AddSpline(socket2.x + socket2.width / 2, socket2.y + socket2.height / 2, mouse.x, mouse.y, 0xFF007FFF);
				break;
		}
	}
	
	buffer.DecrementZoffset();
}

bool GuiFamilyTree::CheckInputImplPost(shared_ptr<Input> input, bool inputEaten)
{
	bool mouseDown = input->GetMouseButton(LEFT);
	ivec2 mouse = input->GetMousePos();
	
	if (input->GetKeyPressed(VK_F1))
	{
		shared_ptr<GuiFamilyTreeNode> node = make_shared<GuiFamilyTreeNode>(50, 50);
		AddComponent(node);
		node->Init();
		node->del->onAction.Add(this, &GuiFamilyTree::OnDel);
	}
	
	if (inputEaten)
		return false;

	if (connectingFromSocketId != -1 && input->GetKeyPressed(VK_ESCAPE))
	{
		connectingFromSocketId = -1;
		connectingFrom = nullptr;
	}
	
	if (mouseDown && !prevMouseState)
	{
		for (int i = components.size() - 1; i >= 0; i--)
		{
			shared_ptr<GuiFamilyTreeNode> node = dynamic_pointer_cast<GuiFamilyTreeNode>(components[i]);
			if (node == nullptr)
				continue;

			ivec2 offset = ivec2(node->boundingBox.x, node->boundingBox.y);
			ivec2 size = ivec2(node->boundingBox.width, node->boundingBox.height);

			GuiArea socket0 = GuiArea(offset.x + size.x / 2 - 5, offset.y - 5, 10, 10);
			GuiArea socket1 = GuiArea(offset.x + size.x / 4, offset.y + size.y - 5, 10, 10);
			GuiArea socket2 = GuiArea(offset.x + size.x / 4 + size.x / 2, offset.y + size.y - 5, 10, 10);

			if (socket0.HitTestAbs(mouse))
			{
				if (node->child != nullptr)
				{
					if (node->child->parent1 == node.get())
						node->child->parent1 = nullptr;
					else if (node->child->parent2 == node.get())
						node->child->parent2 = nullptr;

					node->child = nullptr;
				}

				if (connectingFromSocketId == -1)
				{
					connectingFromSocketId = 0;
					connectingFrom = node;
				}
				else
				{
					if (connectingFromSocketId == 1)
					{
						connectingFrom->parent1 = node.get();
						node->child = connectingFrom.get();
					}
					else if (connectingFromSocketId == 2)
					{
						connectingFrom->parent2 = node.get();
						node->child = connectingFrom.get();
					}

					connectingFromSocketId = -1;
					connectingFrom = nullptr;
				}
			}
			else if (socket1.HitTestAbs(mouse))
			{
				if (node->parent1 != nullptr)
				{
					if (node->parent1->child == node.get())
						node->parent1->child = nullptr;

					node->parent1 = nullptr;
				}

				if (connectingFromSocketId == -1)
				{
					connectingFromSocketId = 1;
					connectingFrom = node;
				}
				else
				{
					if (connectingFromSocketId == 0)
					{
						connectingFrom->child = node.get();
						node->parent1 = connectingFrom.get();
					}

					connectingFromSocketId = -1;
					connectingFrom = nullptr;
				}
			}
			else if (socket2.HitTestAbs(mouse))
			{
				if (node->parent2 != nullptr)
				{
					if (node->parent2->child == node.get())
						node->parent2->child = nullptr;

					node->parent2 = nullptr;
				}

				if (connectingFromSocketId == -1)
				{
					connectingFromSocketId = 2;
					connectingFrom = node;
				}
				else
				{
					if (connectingFromSocketId == 0)
					{
						connectingFrom->child = node.get();
						node->parent2 = connectingFrom.get();
					}

					connectingFromSocketId = -1;
					connectingFrom = nullptr;
				}
			}
		}
	}

	prevMouseState = mouseDown;
	return false;
}

void GuiFamilyTree::OnDel(GuiComponent * node)
{
	for (int i = 0; i < components.size(); i++)
	{
		if (components[i].get() == node->parent.lock().get())
		{
			DisconnectNode((GuiFamilyTreeNode *) components[i].get());
			RemoveComponent(i);
			break;
		}
	}
}

void GuiFamilyTree::DisconnectNode(GuiFamilyTreeNode * node)
{
	if (node->child)
	{
		if (node->child->parent1 == node)
			node->child->parent1 = nullptr;
		else if (node->child->parent2 == node)
			node->child->parent2 = nullptr;

		node->child = nullptr;
	}

	if (node->parent1)
		node->parent1->child = nullptr;
	if (node->parent2)
		node->parent2->child = nullptr;

	node->parent1 = nullptr;
	node->parent2 = nullptr;
}

int GuiFamilyTree::IndexOf(GuiFamilyTreeNode * node)
{
	for (int i = 0; i < components.size(); i++)
	{
		if (components[i].get() == node)
			return i;
	}

	return -1;
}