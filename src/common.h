#pragma once

#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <Windows.h>
#include <CommDlg.h>
#include <windowsx.h>
#include <WinSock2.h>
#include <ws2tcpip.h>

#define _USE_MATH_DEFINES
#include <cmath>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <queue>
#include <set>
#include <stack>

#include <Lib\Glew\glew.h>
#include <Lib\Glew\wglew.h>
#include <Lib\LodePNG\lodepng.h>

#include <Util\Profiling\Timer.h>

using namespace std;