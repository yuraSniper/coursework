#pragma once

#include <common.h>
#include <Main\Main.h>
#include <Window\Window.h>

class SelectFileDialog
{
public:
	static string OpenFile(char * filter)
	{
		char filename[4096] = "";
		OPENFILENAME of = {0};
		of.lStructSize = sizeof(OPENFILENAME);
		of.hwndOwner = Main::GetInstance().mainWindow->GetHWND();
		of.lpstrFilter = filter;
		of.nFilterIndex = 1;
		of.lpstrFile = filename;
		of.nMaxFile = 4096;
		of.lpstrInitialDir = "";
		of.lpstrTitle = "Open";
		of.Flags = OFN_DONTADDTORECENT | OFN_NOCHANGEDIR | OFN_FILEMUSTEXIST | OFN_LONGNAMES | OFN_PATHMUSTEXIST;
		of.lpstrDefExt = "";

		if (GetOpenFileName(&of))
			return string(filename);

		return "";
	}

	static string SaveFile(char * filter)
	{
		char filename[4096] = "";
		OPENFILENAME of = {0};
		of.lStructSize = sizeof(OPENFILENAME);
		of.hwndOwner = Main::GetInstance().mainWindow->GetHWND();
		of.lpstrFilter = filter;
		of.nFilterIndex = 1;
		of.lpstrFile = filename;
		of.nMaxFile = 4096;
		of.lpstrInitialDir = "";
		of.lpstrTitle = "Save";
		of.Flags = OFN_DONTADDTORECENT | OFN_NOCHANGEDIR | OFN_LONGNAMES | OFN_PATHMUSTEXIST;
		of.lpstrDefExt = "";

		if (GetSaveFileName(&of))
			return string(filename);
		return "";
	}
};