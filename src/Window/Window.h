#pragma once

#include <common.h>
#include <Util\Delegate.h>

class Window
{
	HWND hWnd;
	HINSTANCE hInst;
	HDC hDC;
	int width, height;
	void SetupWindowClass();
	static LRESULT WndProc(HWND, UINT, WPARAM, LPARAM);
public:
	Delegate<char> onKey;
	Delegate<int> onMouseWheel;
	Delegate<int> onChar;
	Delegate<> onCLose;

	Window(int width, int height);
	HWND GetHWND();
	HDC GetHDC();
	bool MessageLoop();

	void PutStrInClipboard(u16string str);
	u16string GetStrFromClipboard();

	void SetVisibility(bool visibility);
	void SetMouseCapture(bool visibility);
	void SetTitle(u16string title);
	bool IsVisible();
	bool IsCursorVisible();
	bool IsActive();
	int GetClientWidth();
	int GetClientHeight();
	void SwapBuffers();
};