#pragma once

#include <common.h>
#include <glm.h>

class Window;

enum Mouse
{
	LEFT = VK_LBUTTON,
	RIGHT = VK_RBUTTON,
	MIDDLE = VK_MBUTTON,
	X1 = VK_XBUTTON1,
	X2 = VK_XBUTTON2
};

class Input
{
	shared_ptr<Window> wnd;
	bool keyPressedState[256];
	unsigned char lastKey;
	unsigned int lastChar;
	long long dw;

	void OnKey(char key);
	void OnMouseWheel(int d);
	void OnChar(int c);
public:
	Input(shared_ptr<Window> w);

	bool GetKey(unsigned char key);
	bool GetKeyPressed(unsigned char key);
	unsigned char GetLastKeyDown();
	unsigned char GetLastKeyPressed();
	unsigned int GetLastChar();

	bool GetMouseButton(Mouse button);
	bool GetMouseButtonPressed(Mouse button);
	
	void SetClientCursorPos(int x, int y);
	void SetScreenCursorPos(int x, int y);
	ivec2 GetMousePos();
	int GetMouseWheel();
};