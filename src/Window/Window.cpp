#include "Window.h"

void Window::SetupWindowClass()
{
	WNDCLASSEXW wc = {0};
	wc.cbSize = sizeof(wc);
	wc.style = 0;
	wc.hbrBackground = nullptr;//CreateSolidBrush(0);
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hInstance = hInst = GetModuleHandle(nullptr);
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.lpszClassName = L"Test Window";

	RegisterClassExW(&wc);
}

LRESULT Window::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	POINT mouse;
	Window & This = *(Window *) GetPropW(hWnd, L"This");

	mouse.x = GET_X_LPARAM(lParam);
	mouse.y = GET_Y_LPARAM(lParam);

	switch (msg)
	{
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
			This.onKey(VK_LBUTTON);
			return 0;

		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
			This.onKey(VK_RBUTTON);
			return 0;

		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
			This.onKey(VK_MBUTTON);
			return 0;

		case WM_XBUTTONDOWN:
		case WM_XBUTTONUP:
			This.onKey(VK_XBUTTON1 + HIWORD(wParam));
			return 0;

		case WM_KEYDOWN:
		case WM_KEYUP:
			This.onKey(wParam);
			return 0;

		case WM_CHAR:
			This.onChar(wParam);
			return 0;

		case WM_SYSCOMMAND:
			if (wParam == SC_KEYMENU && GET_Y_LPARAM(lParam) <= 0)
				return 0;
			break;

		case WM_ERASEBKGND:
			return 0;

		case WM_MOUSEWHEEL:
			This.onMouseWheel(GET_WHEEL_DELTA_WPARAM(wParam));
			return 0;
			
		case WM_DESTROY:
			This.onCLose();
			return 0;
	}
	return DefWindowProcW(hWnd, msg, wParam, lParam);
}

Window::Window(int width, int height)
{
	SetupWindowClass();
	RECT rt, rt1;
	GetWindowRect(GetDesktopWindow(), &rt1);

	int style = WS_OVERLAPPEDWINDOW;
	int exStyle = 0;

	rt.left = rt.top = 0;
	rt.right = width;
	rt.bottom = height;
	AdjustWindowRectEx(&rt, style, 0, exStyle);
	width = rt.right - rt.left;
	height = rt.bottom - rt.top;

	hWnd = CreateWindowExW(exStyle, L"Test Window", L"", style,
		(rt1.right - width) / 2, (rt1.bottom - height) / 2,
		width, height, nullptr, nullptr, hInst, nullptr);
	
	SetPropW(hWnd, L"This", this);

	hDC = GetDC(hWnd);
	PIXELFORMATDESCRIPTOR pfd = {0};
	pfd.nVersion = 1;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 16;
	pfd.cStencilBits = 16;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;

	SetPixelFormat(hDC, ChoosePixelFormat(hDC, &pfd), &pfd);
}

HWND Window::GetHWND()
{
	return hWnd;
}

HDC Window::GetHDC()
{
	return hDC;
}

bool Window::MessageLoop()
{
	MSG msg;
	if (PeekMessageW(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		if (msg.message == WM_QUIT)
			return false;
		TranslateMessage(&msg);
		DispatchMessageW(&msg);
	}
	return true;
}

void Window::SetVisibility(bool visibility)
{
	ShowWindow(hWnd, visibility? SW_SHOW : SW_HIDE);
}

void Window::SetMouseCapture(bool visibility)
{
	ShowCursor(visibility);
}

void Window::SetTitle(u16string title)
{
	SetWindowTextW(hWnd, (wchar_t *)title.c_str());
}

bool Window::IsVisible()
{
	WINDOWPLACEMENT wp;
	wp.length = sizeof(wp);
	GetWindowPlacement(hWnd, &wp);
	return wp.showCmd != SW_HIDE && wp.showCmd != SW_MINIMIZE;
}

bool Window::IsCursorVisible()
{
	CURSORINFO ci;
	ci.cbSize = sizeof(ci);
	GetCursorInfo(&ci);
	return ci.flags != 0;
}

bool Window::IsActive()
{
	return GetForegroundWindow() == hWnd;
}

int Window::GetClientWidth()
{
	RECT rt;
	GetClientRect(hWnd, &rt);
	return rt.right - rt.left;
}

int Window::GetClientHeight()
{
	RECT rt;
	GetClientRect(hWnd, &rt);
	return rt.bottom - rt.top;
}

void Window::SwapBuffers()
{
	::SwapBuffers(hDC);
}

void Window::PutStrInClipboard(u16string str)
{
	while (!OpenClipboard(hWnd));
	EmptyClipboard();
	HANDLE mem = GlobalAlloc(GMEM_MOVEABLE, str.length() * 2 + 2);
	void * ptr = GlobalLock(mem);

	char16_t * tmp = (char16_t *)ptr;

	for (int i = 0; i < str.length(); i++)
		tmp[i] = str[i];
	tmp[str.length()] = 0;

	//memcpy(ptr, str.c_str(), wcslen((wchar_t *)str.c_str()) + 1);
	
	SetClipboardData(CF_UNICODETEXT, ptr);
	GlobalUnlock(mem);
	CloseClipboard();
}

u16string Window::GetStrFromClipboard()
{
	u16string str;
	while (!OpenClipboard(hWnd));
	if (IsClipboardFormatAvailable(CF_UNICODETEXT))
	{
		HANDLE mem = GetClipboardData(CF_UNICODETEXT);
		void * ptr = GlobalLock(mem);
		
		str.assign((char16_t *) ptr);
		
		GlobalUnlock(mem);
	}

	CloseClipboard();

	return str;
}