#include <common.h>
#include <Main\Main.h>

#pragma comment(lib, "Ws2_32.lib")

int main()
//int _stdcall WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	Main::GetInstance().Start();
	ExitProcess(0);
	return 0;
}