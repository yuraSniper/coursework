#pragma once

#define GLM_SWIZZLE
#include <Lib\glm\glm.hpp>
#include <Lib\glm\gtc\matrix_transform.hpp>
#include <Lib\glm\gtc\matrix_inverse.hpp>
#include <Lib\glm\gtc\type_ptr.hpp>
#include <Lib\glm\gtx\fast_square_root.hpp>

using namespace glm;