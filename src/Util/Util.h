#pragma once

#include <common.h>

class Util
{
public:
	static double deg2rad(double a)
	{
		return a * M_PI / 180.;
	}

	template<typename T>
	static T sqr(T x)
	{
		return x * x;
	}

	template<typename T>
	static T wrap(T x, T val)
	{
		T tmp = x % val;
		return tmp < 0 ? val + tmp : tmp;
	}

	template<typename T>
	static T clamp(T val, T min, T max)
	{
		return val < min ? min : val > max ? max : val;
	}

	template<typename T>
	static bool isInRange(T val, T min, T max)
	{
		return val > min && val < max;
	}

	template<typename T>
	static char compareToRange(T val, T min, T max)
	{
		return val < min ? -1 : val > max ? 1 : 0;
	}
};