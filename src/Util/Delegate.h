#pragma once

#include <vector>
#include <memory>
using namespace std;

template<class ... ARGS>
class Delegate
{
	struct Tmp
	{
		virtual void invoke(ARGS ... args) = 0;
	};

	template<class T>
	struct Function : public Tmp
	{
		void (T::*f)(ARGS ...);
		T * obj;
		Function(void(T::*f)(ARGS ...), T * param)
		{
			this->f = f;
			this->obj = param;
		}

		virtual void invoke(ARGS ... args)
		{
			(obj->*f)(args ...);
		}
	};

	vector<unique_ptr<Tmp>> list;
public:
	template<class T>
	void Add(T * param, void (T::*f)(ARGS ...))
	{
		list.push_back(make_unique<Function<T>>(f, param));
	}

	void operator()(ARGS ... args)
	{
		for (int i = 0; i < list.size(); i++)
			list[i]->invoke(args ...);
	}

	void Clear()
	{
		list.clear();
	}
};