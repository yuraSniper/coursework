#include "ByteStream.h"

vector<unsigned char> & ByteStream::GetData()
{
	return data;
}

void ByteStream::SetData(vector<unsigned char> & data)
{
	this->data = data;
	readPointer = 0;
}

int ByteStream::GetStreamLength()
{
	return data.size();
}

bool ByteStream::Eof()
{
	return readPointer >= data.size();
}

void ByteStream::Write(void * p, int size)
{
	data.insert(data.end(), (unsigned char *)p, (unsigned char *)p + size);
}

void ByteStream::Write(string str)
{
	Write((int) str.size());
	Write((void *)str.data(), str.size());
}

void ByteStream::Write(u16string & str)
{
	Write((int) str.size() * 2);
	Write((void *) str.data(), str.size() * 2);
}

void ByteStream::Write(vector<unsigned char> & vec)
{
	Write((int) vec.size());
	Write((void *) vec.data(), vec.size());
}

int ByteStream::Read(void * p, int size)
{
	int bytesRead = (data.size() - readPointer);
	if (bytesRead > size)
		bytesRead = size;
	else if (bytesRead < size)
		bytesRead = 0;

	for (int i = 0; i < bytesRead; i++, readPointer++)
		((unsigned char *)p)[i] = *(data.data() + readPointer);

	return bytesRead;
}

void ByteStream::Read(string & str)
{
	int size;
	char *p;
	Read(size);
	p = new char[size];
	Read(p, size);
	str.assign(p, size);
	delete p;
}

void ByteStream::Read(u16string & str)
{
	int size;
	void * p;
	Read(size);
	p = new char[size];
	Read(p, size);
	str.assign((char16_t *) p, size / 2);
	delete p;
}

void ByteStream::Read(vector<unsigned char> & vec)
{
	int size;
	unsigned char *p;
	Read(size);
	p = new unsigned char[size];
	Read(p, size);
	vec.assign(p, p + size);
	delete p;
}

void ByteStream::Write(char c)
{
	Write(&c, sizeof(char));
}

void ByteStream::Write(int i)
{
	Write(&i, sizeof(int));
}

void ByteStream::Read(char & c)
{
	Read(&c, sizeof(char));
}

void ByteStream::Read(int & i)
{
	Read(&i, sizeof(int));
}