#pragma once

#include <common.h>

class ByteStream
{
	vector<unsigned char> data;
	int readPointer = 0;
public:
	vector<unsigned char> & GetData();
	void SetData(vector<unsigned char> & data);
	int GetStreamLength();

	bool Eof();

	void Write(void * p, int size);
	int Read(void * p, int size);

	void Write(char c);
	void Write(int i);
	void Write(string str);
	void Write(u16string & str);
	void Write(vector<unsigned char> & vec);

	void Read(char & c);
	void Read(int & i);
	void Read(string & str);
	void Read(u16string & str);
	void Read(vector<unsigned char> & vec);
};