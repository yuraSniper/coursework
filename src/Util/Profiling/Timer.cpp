#include "Timer.h"

Timer::Timer()
{
	QueryPerformanceFrequency(&freq);
	start();
}

void Timer::start()
{
	QueryPerformanceCounter(&startPoint);
}

long long Timer::diff(int mult)
{
	LARGE_INTEGER tmp;
	QueryPerformanceCounter(&tmp);
	return (tmp.QuadPart - startPoint.QuadPart) * mult / freq.QuadPart;
}