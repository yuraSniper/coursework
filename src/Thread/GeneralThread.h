#pragma once

#include <Thread\Thread.h>
#include <Util\Delegate.h>

class GeneralThread : public Thread
{
	bool working = true;
public:
	Delegate<> callback;
	virtual void run()
	{
		while (working)
		{
			callback();
		}
	}
};