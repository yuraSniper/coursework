#pragma once

#include <common.h>

class Task
{
public:
	int priority;

	Task()
	{
		priority = 0;
	}

	virtual void operate() = 0;

	virtual bool operator<(Task & t)
	{
		return priority > t.priority;
	}
};