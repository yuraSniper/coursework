#pragma once

#include <common.h>
#include <Thread\Task\Task.h>

class TaskComparator
{
public:
	bool operator()(Task * a, Task * b)
	{
		return a < b;
	}
};