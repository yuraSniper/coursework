#pragma once

#include <common.h>
#include <Thread\Task\Task.h>
#include <Thread\Task\TaskComparator.h>
#include <Thread\CriticalSection.h>

class Thread
{
	HANDLE hThread;
	static int callback(void * param)
	{
		((Thread *)param)->run();
		return 0;
	}
protected:
	set<Task *, TaskComparator> taskQueue;
public:
	CriticalSection crit;

	Thread()
	{
		hThread = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)callback, this, CREATE_SUSPENDED, nullptr);
		crit.init();
	}

	void scheduleTask(Task * task, int priority = 0)
	{
		task->priority = priority;
		crit.lock();
		taskQueue.insert(task);
		crit.unlock();
	}

	virtual void run() = 0;

	void finalize()
	{
		terminate();
	}

	void terminate()
	{
		TerminateThread(hThread, 0);
	}

	void suspend()
	{
		SuspendThread(hThread);
	}

	void resume()
	{
		ResumeThread(hThread);
	}
};