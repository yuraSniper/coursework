#Vertex
#version 330

uniform vec2 offset;

layout(location = 0) in vec2 loc;
layout(location = 1) in vec4 color;
layout(location = 2) in int c;

smooth out vec4 vColor;
smooth out float vSize;
flat out int vC;

void main()
{
	gl_Position = vec4(offset.x + loc.x, offset.y, 0.0, 1.0);
	vSize = loc.y;
	vColor = color;
	vC = c;
}

#Geometry
#version 330

uniform mat4 mvp;
uniform float size;
uniform vec2 minCorner;
uniform vec2 maxCorner;
uniform bool useBounds;

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

smooth in vec4 vColor[];
smooth in float vSize[];
flat in int vC[];

smooth out vec2 texCoord;
smooth out vec4 color;
flat out int c;

vec2 unmix(vec2 a, vec2 b, vec2 x)
{
	return (x - a) / (b - a);
}

void main()
{
	vec2 p1 = gl_in[0].gl_Position.xy;
	vec2 p2 = p1 + vec2(vSize[0] * size, size);

	vec2 t1 = vec2((1 - vSize[0]) * 0.5f, 0);
	vec2 t2 = vec2((1 + vSize[0]) * 0.5f, 1);

	if (useBounds)
	{
		vec2 newp1 = clamp(p1, minCorner, maxCorner);
		vec2 newp2 = clamp(p2, minCorner, maxCorner);

		vec2 newt1 = mix(t1, t2, unmix(p1, p2, newp1));
		vec2 newt2 = mix(t1, t2, unmix(p1, p2, newp2));

		p1 = newp1;
		p2 = newp2;
		t1 = newt1;
		t2 = newt2;
	}

	gl_Position = mvp * vec4(p1, 0.0, 1.0);
	color = vColor[0];
	texCoord = t1;
	c = vC[0];
	EmitVertex();

	gl_Position = mvp * vec4(p1.x, p2.y, 0.0, 1.0);
	color = vColor[0];
	texCoord = vec2(t1.x, t2.y);
	c = vC[0];
	EmitVertex();

	gl_Position = mvp * vec4(p2.x, p1.y, 0.0, 1.0);
	color = vColor[0];
	texCoord = vec2(t2.x, t1.y);
	c = vC[0];
	EmitVertex();

	gl_Position = mvp * vec4(p2, 0.0, 1.0);
	color = vColor[0];
	texCoord = t2;
	c = vC[0];
	EmitVertex();

	EndPrimitive();
}

#Fragment
#version 330

uniform sampler2DArray tex;
uniform float minAlpha;
uniform float maxAlpha;

smooth in vec4 color;
smooth in vec2 texCoord;
flat in int c;

layout(location = 0) out vec4 outColor;

void main()
{
	int row = c / 16;
	int column = c % 16;

	vec2 minUV = vec2(column / 16.0, row / 16.0);

	vec2 coord = texCoord - floor(texCoord);
	float alpha = texture(tex, vec3(minUV + coord / 16.0, c >> 8)).r;
	alpha = smoothstep(minAlpha, maxAlpha, alpha);
	vec4 tmpColor = vec4(1, 1, 1, alpha) * color;

	outColor = tmpColor;
}