#Vertex
#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
layout(location = 2) in vec2 texCoord;
layout(location = 3) in ivec2 texPos;

smooth out vec2 vTexCoord;
smooth out vec4 vColor;
flat out ivec2 vTexPos;

void main()
{
	gl_Position = vec4(position, 1);
	vTexCoord = texCoord;
	vTexPos = texPos;
	vColor = color;
}

#Geometry
#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform mat4 mvp;

smooth in vec2 vTexCoord[];
smooth in vec4 vColor[];
flat in ivec2 vTexPos[];

smooth out vec2 gTexCoord;
smooth out vec4 gColor;
flat out ivec2 gTexPos;
smooth out vec3 gNormal;

void main()
{
	for (int i = 0; i < 3; i++)
	{
		gl_Position = mvp * gl_in[i].gl_Position;
		gTexCoord = vTexCoord[i];
		gColor = vColor[i];
		gTexPos = vTexPos[i];
		gNormal = normalize(cross(gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz, gl_in[2].gl_Position.xyz - gl_in[1].gl_Position.xyz));

		EmitVertex();
	}
	EndPrimitive();
}

#Fragment
#version 330

uniform sampler2DArray tex;
uniform int[5] texRowCount;

smooth in vec2 gTexCoord;
smooth in vec4 gColor;
flat in ivec2 gTexPos;
smooth in vec3 gNormal;

layout(location = 0) out vec4 color;

void main()
{
	int row = gTexPos.y / texRowCount[gTexPos.x];
	int column = gTexPos.y % texRowCount[gTexPos.x];

	vec2 minUV = vec2(column / float(texRowCount[gTexPos.x]), row / float(texRowCount[gTexPos.x]));

	vec2 coord = gTexCoord - floor(gTexCoord);
	color = gColor * texture(tex, vec3(minUV + coord / float(texRowCount[gTexPos.x]), gTexPos.x));
}