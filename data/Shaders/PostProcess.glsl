#Vertex
#version 330

uniform mat4 mvp;

layout(location = 0) in vec3 position;
layout(location = 2) in vec2 texCoord;

smooth out vec2 vTexCoord;

void main()
{
	gl_Position = mvp * vec4(position, 1.0);
	vTexCoord = texCoord;
}

#Fragment
#version 330

uniform sampler2D colorBuffer;
uniform sampler2D depthBuffer;

smooth in vec2 vTexCoord;

layout(location = 0) out vec4 color;

float getLinearDepth(vec2 coord)
{
	float n = 0.1;
	float f = 1000.0;
	float z = texture(depthBuffer, coord).x;
	return (2.0 * n) / (f + n - z * (f - n));
}

void main()
{
	vec2 coord = vTexCoord;
	coord.y = 1 - coord.y;

	//float d = getLinearDepth(coord);
	float d = texture(depthBuffer, coord * 4).x;
	//color = vec4(d, d, d, 1) * texture(colorBuffer, vTexCoord);
	color = texture(colorBuffer, coord);
}