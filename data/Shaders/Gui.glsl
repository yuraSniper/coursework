#Vertex
#version 330

uniform mat4 mvp;

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
layout(location = 2) in vec2 texCoord;
layout(location = 3) in float alpha;
layout(location = 4) in int c;

smooth out vec4 vColor;
smooth out vec2 vTexCoord;
smooth out float vAlphaThreshold;
flat out int vC;

void main()
{
	gl_Position = mvp * vec4(position, 1);
	vColor = color;
	vTexCoord = texCoord;
	vAlphaThreshold = alpha;
	vC = c;
}

#Fragment
#version 330

uniform sampler2DArray tex;

smooth in vec4 vColor;
smooth in vec2 vTexCoord;
smooth in float vAlphaThreshold;
flat in int vC;

layout(location = 0) out vec4 color;

void main()
{
	int row = vC / 16;
	int column = vC % 16;

	vec2 minUV = vec2(column / 16.0, row / 16.0);

	vec2 coord = vTexCoord - floor(vTexCoord);
	float alpha = mix(1, texture(tex, vec3(minUV + coord / 16.0, vC >> 8)).r, vC != 0);
	alpha = smoothstep(0.5f - vAlphaThreshold / 2.0f, 0.5f + vAlphaThreshold / 2.0f, alpha);
	vec4 tmpColor = vec4(1, 1, 1, alpha) * vColor;

	color = tmpColor;
}