#Vertex
#version 330

uniform mat4 mvp;

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;

out vec4 vColor;

void main()
{
	gl_Position = mvp * vec4(position, 1.0);
	vColor = color;
}

#Fragment
#version 330

in vec4 vColor;

layout(location = 0) out vec4 color;

void main()
{
	color = vColor;
	//color = vec4(1, 1, 1, 1);
}